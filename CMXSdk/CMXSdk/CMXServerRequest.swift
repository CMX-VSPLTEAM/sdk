//
//  CMXServerRequest.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */



//MARK: - CMXServerRequest : This class handles communication of application with its Server.

import Foundation

//MARK: - Url's
struct ServerSupportURLS {
    static let CHECK_CMX_SERVER_VALIDITY = "/api/config/v1/alerts/count"
    static let GET_CMX_SERVER_SERVICES = "/api/config/v1/services"
    static let GET_CMX_SERVER_VERSION = "/api/config/v1/version/image"
    static let GET_USER_LIVE_LOCATION = "/api/location/v2/clients"
    static let GET_USER_MAP = "/api/location/v2/clients"
    static let GET_MAP_FLOOR_PATH = "/api/location/v2/clients"
}

class CMXServerRequest : CMXApiClient {
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func checkIfCMXServerValid(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.CHECK_CMX_SERVER_VALIDITY) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getCMXServerServices(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_CMX_SERVER_SERVICES) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getCMXServerVersion(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_CMX_SERVER_VERSION) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getUserLocationFromCMX(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            cmxhfCopyData(information, sourceKey: "macAddress", destinationDictionary: parameters, destinationKey: "macAddress", methodName:#function)
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_USER_LIVE_LOCATION) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getImageData(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlockForFile) -> () {
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String , let url = information.object(forKey: "url") as? NSString {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            addCommonInformation(parameters)
            performDownloadGetRequest(url, completionBlock: completionBlock, methodName: #function)
        }
    }
    
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getMapFloorPath(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        let jsonFileName = CMXCfg().floorPathResourceFileName
        let filePath = Bundle.main.path(forResource: jsonFileName, ofType: nil)
        do{
        let jsonString = try NSString(contentsOf: NSURL(fileURLWithPath: filePath!) as URL, encoding: String.Encoding.utf8.rawValue)
        completionBlock(jsonString as AnyObject?)
        }catch{}
        return
        let parameters = NSMutableDictionary()
        if let ip = information.object(forKey: "ip") as? String {
            authenticationUserName = information.object(forKey: "userName") as! String
            authenticationPassword  = information.object(forKey: "password") as! String
            cmxhfCopyData(information, sourceKey: "macAddress", destinationDictionary: parameters, destinationKey: "macAddress", methodName:#function)
            addCommonInformation(parameters)
            performGetRequest(parameters, urlString: String("https://"+ip+ServerSupportURLS.GET_USER_LIVE_LOCATION) as NSString, completionBlock: completionBlock,methodName:#function)
        }
    }

}


