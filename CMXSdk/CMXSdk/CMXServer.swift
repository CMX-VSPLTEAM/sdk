//
//  CMXServer.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

// Content Description : This is a DataModel to hold details the cmx server

import Foundation

public enum ServerStatus {
    case UnKnown
    case Running
    case NotRunning
}

/// <#Description#>
public class CMXServer: NSObject {
    
    /// <#Description#>
    public  var ip : String?
    /// <#Description#>
    public  var name : String?
    /// <#Description#>
    public  var userName : String?
    /// <#Description#>
    public  var password : String?
    /// <#Description#>
    public  var version : String?
    /// <#Description#>
    public  var services = [CMXService]()
    /// <#Description#>
    public  var status = ServerStatus.Running
    
    /// <#Description#>
    ///
    /// - parameter ip:       <#ip description#>
    /// - parameter name:     <#name description#>
    /// - parameter userName: <#userName description#>
    /// - parameter password: <#password description#>
    ///
    /// - returns: <#return value description#>
    init(ip:String,name:String,userName:String,password:String) {
        self.ip = ip
        self.name = name
        self.password = password
        self.userName = userName
    }
}
