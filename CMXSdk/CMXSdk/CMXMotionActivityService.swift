//
//  CMXMotionActivityService.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion

class CMXMotionActivityService: NSObject {
    
    var activityMotionStateArray = [CMXMotion]()
    var activityArray = [CMXActivity]()
    var activityConfidenceArray = [CMXMotionActivityConfidence]()
    var lastMotionActivity : CMXMotionActivity?
    
    func startProcessing(){
        Sc().startActivityUpdates {[weak self] (activity) in guard let `self` = self else { return }
            if activity.motionActivity.automotive {
                self.activityArray.append(CMXActivity(activityType: .Automotive, timeStamp: Date()))
            }else if activity.motionActivity.unknown {
                self.activityArray.append(CMXActivity(activityType: .Unknown, timeStamp: Date()))
            }else if activity.motionActivity.stationary {
                self.activityArray.append(CMXActivity(activityType: .Stationary, timeStamp: Date()))
            }else if activity.motionActivity.walking {
                self.activityArray.append(CMXActivity(activityType: .Walking, timeStamp: Date()))
            }else if activity.motionActivity.running {
                self.activityArray.append(CMXActivity(activityType: .Running, timeStamp: Date()))
            }else if activity.motionActivity.cycling {
                self.activityArray.append(CMXActivity(activityType: .Cycling, timeStamp: Date()))
            }
            self.activityConfidenceArray.append(CMXMotionActivityConfidence(confidence: activity.motionActivity.confidence, timeStamp: Date()))
            self.lastMotionActivity = activity
            
            if self.activityArray.count > 0{
                if self.activityArray.last!.activityType == CMXActivityType.Unknown || self.activityArray.last!.activityType == CMXActivityType.Stationary {
                    self.activityMotionStateArray.append((CMXMotion(motionState: .Stationary, originalCadence: 0.0 , pace:  0.0 , timeStamp: Date())))
                }else{
                    self.activityMotionStateArray.append((CMXMotion(motionState: .Moving, originalCadence: 0.0 , pace:  0.0 , timeStamp: Date())))
                }
            }
        }
    }
}
