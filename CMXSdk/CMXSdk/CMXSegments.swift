//
//  CMXSegments.swift
//
//  Created by  on 19/10/16
//  Copyright (c) . All rights reserved.
//

import Foundation

public class CMXSegments: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kCMXSegmentsAngleFromXAxisKey: String = "angleFromXAxis"
	internal let kCMXSegmentsStartEndNodesKey: String = "startEndNodes"


    // MARK: Properties
	public var angleFromXAxis: Float?
	public var startEndNodes: [NSNumber]?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		angleFromXAxis = json[kCMXSegmentsAngleFromXAxisKey].float
		startEndNodes = []
		if let items = json[kCMXSegmentsStartEndNodesKey].array {
			for item in items {
				startEndNodes?.append(NSNumber(value: item.int!))
			}
		} else {
			startEndNodes = nil
		}

    }

    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    required public init?(map: Map){

    }

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
		angleFromXAxis <- map[kCMXSegmentsAngleFromXAxisKey]
		startEndNodes <- map[kCMXSegmentsStartEndNodesKey]

    }
}
