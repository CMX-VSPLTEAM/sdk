//
//  CMXCommunication.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation


/// Manager class dealling with every type communication with CMX Server
class CMXCommunication: NSObject {
    /// initialize instance
    static let shared : CMXCommunication = {
        let instance = CMXCommunication()
        return instance
    }()
    
    var serverRequestObject1 = CMXServerRequest()
    var serverRequestObject2 = CMXServerRequest()
    var serverRequestObject3 = CMXServerRequest()
    var serverRequestObject4 = CMXServerRequest()
    var serverRequestObject5 = CMXServerRequest()
    var serverRequestObject6 = CMXServerRequest()

    override init() {
        
    }
    
}

extension CMXCommunication {
    
    /// IsLoggedIn
    public func IsLoggedIn()->Bool{
        return Auth().isLoggedIn
    }
    
    /// activeServer
    /// returns Active Server Object if Exist
    public func activeServer()->CMXServer?{
        return Auth().server
    }
    
    /// Logout
    /// Calling this will clear everything and stop any already running fetches and updates
    public func logOut(){
        Auth().logOut()
    }
    
    /// Authenticate CMX server
    ///
    /// - parameter ip:         ip address of server
    /// - parameter name:       name (it can be any name, user internally for tagging)
    /// - parameter userName:   userName to login with
    /// - parameter password:   password to authenticate login
    /// - parameter completion: completion callback
    /// - parameter failure:    failure callback
    public func authenticate(ip:String,name:String,userName:String,password:String,completion:@escaping CMXASuccessBlock,failure:@escaping CMXAFailureBlock) {
        Auth().authenticate(ip: ip, name: name, userName: userName, password: password, completion: completion, failure: failure)
    }
    
    /// This load the image resources like map floor image
    ///
    /// - parameter imageName:  name of image
    /// - parameter completion: completion
    public func getImageData(_ imageName:String,_ completion:@escaping CMXACCompletionBlockForFile) {
        assert(Auth().isLoggedIn,"please authenticate first before using any services")
        let serverRequest = CMXServerRequest()
        if let cachedImageData = UserDefaults.standard.object(forKey: imageName) as? NSData {
            completion(cachedImageData);return
        }
        let urlString = "https://\(cmxhfSafeString(Auth().server!.ip))/api/config/v1/maps/imagesource/\(imageName)"
        let information = NSMutableDictionary()
        information.setObject(Auth().server!.ip!, forKey: "ip" as NSCopying)
        information.setObject(Auth().server!.userName!, forKey: "userName" as NSCopying)
        information.setObject(Auth().server!.password!, forKey: "password" as NSCopying)
        information.setObject(Auth().server!.password!, forKey: "password" as NSCopying)
        information.setObject(urlString, forKey: "url" as NSCopying)
        serverRequestObject1 = CMXServerRequest()
        serverRequestObject1.returnFailureResponseAlso = false
        serverRequestObject1.getImageData(information) { [weak self] (data) in guard let `self` = self else { return }
            if data != nil {
                UserDefaults.standard.set((data), forKey: imageName)
            }
            completion(data)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func checkIfCMXServerValid(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        serverRequestObject2 = CMXServerRequest()
        serverRequestObject2.returnFailureResponseAlso = false
        serverRequestObject2.checkIfCMXServerValid(information, completionBlock: completionBlock)
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getCMXServerServices(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        serverRequestObject3 = CMXServerRequest()
        serverRequestObject3.returnFailureResponseAlso = false
        serverRequestObject3.getCMXServerServices(information, completionBlock: completionBlock)
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getCMXServerVersion(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        serverRequestObject4 = CMXServerRequest()
        serverRequestObject4.returnFailureResponseAlso = false
        serverRequestObject4.getCMXServerVersion(information, completionBlock: completionBlock)
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getUserLocationFromCMX(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        serverRequestObject5 = CMXServerRequest()
        serverRequestObject5.returnFailureResponseAlso = true
        serverRequestObject5.getUserLocationFromCMX(information, completionBlock: completionBlock)
    }
    
    /// <#Description#>
    ///
    /// - parameter information:     <#information description#>
    /// - parameter completionBlock: <#completionBlock description#>
    func getMapFloorPathFromCMX(_ information: NSDictionary ,completionBlock: @escaping CMXACCompletionBlock) -> () {
        serverRequestObject6 = CMXServerRequest()
        serverRequestObject6.returnFailureResponseAlso = false
        serverRequestObject6.returnJsonReponse = true
        serverRequestObject6.getMapFloorPath(information, completionBlock: completionBlock)
    }

}
