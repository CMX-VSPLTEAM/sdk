//
//  CMXConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

class CMXConfigurations: NSObject {

    /// initialize instance
    static let shared : CMXConfigurations = {
        let instance = CMXConfigurations()
        return instance
    }()
    
    var cmxLocationUpdateInterval = 2
    var userRawCoordinatesForCMXLocation = false
    var floorPathResourceFileName = "floorpathV1.json"
    
    override init() {
    }
}
