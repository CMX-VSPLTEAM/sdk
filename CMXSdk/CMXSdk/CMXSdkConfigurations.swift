//
//  CMXSdkConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

class CMXAppConfigurations: NSObject {
    
    /// initialize instance
    static let shared : CMXAppConfigurations = {
        let instance = CMXAppConfigurations()
        return instance
    }()
    
    override init() {
    }
    
    #if DEBUG
        public var loggingEnabled = true
    #else
        public var loggingEnabled = true
    #endif
    
    /// <#Description#>
    public var logInfoMessages = true
    
    /// <#Description#>
    public var logErrorMessages = true
    
    /// <#Description#>
    public var logWarningMessages = true
    
    /// <#Description#>
    public var logSuccessMessages = true
    
    /// <#Description#>
    public var logObservationMessages = true
    
    /// <#Description#>
    public var writeDataToCSVFile = true
    
    /// <#Description#>
    public var userLocationCallBackUpdateInterval = 0.2
    
    /// <#Description#>
    let cmxKMaxObservationCount = 500
    
    /// <#Description#>
    let cmxServerConfigurations = CMXCfg()
    
    /// <#Description#>
    let sensorConfigurations = SCfg()
    
    /// <#Description#>
    let internalConfigurations = CfgInternal()
}
