//
//  CMXAcceleration.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion

/// Data modal to hold acceleration
public class CMXAcceleration: CMXCapturableData {
    
    /// <#Description#>
    public var rawData : CMAccelerometerData!
    
    /// <#Description#>
    var timeStamp : Date!
    
    /// <#Description#>
    ///
    /// - parameter activityType: <#activityType description#>
    /// - parameter timeStamp:    <#timeStamp description#>
    ///
    /// - returns: <#return value description#>
    init(rawData:CMAccelerometerData,timeStamp:Date) {
        self.rawData = rawData
        self.timeStamp = timeStamp
    }
}
