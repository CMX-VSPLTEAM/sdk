//
//  CMXBeaconCollection.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreLocation

public class CMXBeaconCollection: CMXCapturableData {

    /// <#Description#>
    var beacons : [CLBeacon]!
    var beaconsFormatted : CMXBeaconCollectionFormatted!

    /// <#Description#>
    var timeStamp : Date!

    /// <#Description#>
    ///
    /// - parameter beacon: <#beacon description#>
    ///
    /// - returns: <#return value description#>
    init(beacons:[CLBeacon]?, timeStamp:Date) {
        self.beacons = beacons
        self.timeStamp = timeStamp
    }
    
    func getFormattedBeaconsData(validMarkers:[CMXBleMarkers]) -> String {
        //Step 1:
        let formattedString = NSMutableString()
        
        //Step 2:
        formattedString.append("\(String(format: "%ld", timeStamp.timeIntervalInMilliSecondsSince1970())),\(validMarkers.count)")
        
        //Step 3:
        for marker in validMarkers {
            formattedString.append("\(marker.majorId!),")
            formattedString.append("\(marker.minorId!),")
            formattedString.append("\(getProximity(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker))),")
            formattedString.append("\(getAccuracy(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker))),")
            formattedString.append("\(getRSSI(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker)))")
        }
        
        //Step 4:
        return formattedString as String
    }
    
    func getFormattedBeaconsDataObject(validMarkers:[CMXBleMarkers]) -> CMXBeaconCollectionFormatted {
        var cmxBeaconsInfo = [CMXBeaconInfo]()
        for marker in validMarkers {
            cmxBeaconsInfo.append(CMXBeaconInfo(minor: marker.minorId!, major: marker.majorId!, prox: getProximity(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker)), accu: getAccuracy(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker)), rssi: getRSSI(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker))))
        }
        self.beaconsFormatted = CMXBeaconCollectionFormatted(beaconsInfo: cmxBeaconsInfo,timeStamp: timeStamp)
        return self.beaconsFormatted!
    }

    func getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker:CMXBleMarkers) -> CLBeacon? {
        if marker.majorId != nil && marker.minorId != nil && beacons != nil {
            for beacon in beacons! {
                if beacon.major != nil && beacon.minor != nil {
                    if beacon.minor.intValue == marker.minorId! {
                        printInfo(data:"marker: \(marker.minorId)")
                        printInfo(data:"beacon: \(beacon.minor.intValue)")
                        return beacon
                    }
                }
            }
        }
        return nil
    }
    
    func getProximity(beacon: CLBeacon?) -> Int {
        if (beacon == nil) { return 0 }
        return Int(beacon!.proximity.rawValue)
    }
    
    func getAccuracy(beacon: CLBeacon?) -> Double {
        if (beacon == nil) { return -1 }
        return Double(beacon!.accuracy)
    }

    func getRSSI(beacon: CLBeacon?) -> Int {
        if (beacon == nil) { return -140 }
        return Int(beacon!.rssi)
    }
    
    func getTimestamp()->Date{
        return timeStamp!
    }
}

