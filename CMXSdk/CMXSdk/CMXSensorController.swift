//
//  CMXSensorController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion


/// Call back handlers , block based
public typealias CMXCLCMFCompletionBlock = (_ magneticField:CMXMagneticField) ->()
public typealias CMXCLCACCompletionBlock = (_ acceleration:CMXAcceleration) ->()
public typealias CMXCLCRCompletionBlock = (_ rotation:CMXRotation) ->()
public typealias CMXCLCATCompletionBlock = (_ attitude:CMXAttitude) ->()
public typealias CMXCLCALCompletionBlock = (_ altitude:CMXAltitude) ->()
public typealias CMXCLCPDCompletionBlock = (_ pedometer:CMPedometerData) ->()
public typealias CMXCLCMACompletionBlock = (_ motionActivity:CMXMotionActivity) ->()
public typealias CMXCLCMASCompletionBlock = (_ motionActivities:[CMXMotionActivity]?) ->()

/// Manager class dealling with every type of motion events
class CMXSensorController: NSObject {
    
    /// initialize instance
    static let shared : CMXSensorController = {
        let instance = CMXSensorController()
        return instance
    }()
    
    override init() {
    }
    
    var motionManager: CMMotionManager?
    /// <#Description#>
    
    var pedometer: CMPedometer?
    /// <#Description#>
    
    var altimeter: CMAltimeter?
    /// <#Description#>
    
    var motionActivityManager: CMMotionActivityManager?
    /// <#Description#>
    
    public func logOut() {
        stopAllUpdates()
    }
    
    public func stopAllUpdates() {
        stopBeaconUpdates()
       // stopBeaconUpdatesFromLoc()
        stopHeadingUpdates()
        stopPedometerUpdates()
        stopAltitudeUpdates()
        stopAttitudeUpdates()
        stopAccelerometerUpdates()
        stopMagnetometerUpdates()
        stopGyrometerUpdates()
        stopActivityUpdates()
        motionManager = nil
        pedometer = nil
        altimeter = nil
        motionActivityManager = nil
    }
}

extension CMXSensorController {
    
    /// Beacons
    func startBeaconsUpdates(_ completion: @escaping CMXBCCompletionBlock, failure: @escaping CMXBCFailureBlock) {
        Bc().scanForBeacons(completion: completion, failure: failure)
    }
    
    func stopBeaconUpdates() {
        Bc().stopScanning()
    }
    
    /// Headings
    func startHeadingUpdates(_ completion: @escaping CMXHCCompletionBlock, failure: @escaping CMXHCFailureBlock) {
        Hc().startUpdatingHeadings(completion: completion, failure: failure)
    }
    
    func stopHeadingUpdates() {
        Hc().stopUpdates()
    }
    
    /// Accelerometer
    func startAccelerometerUpdates(completion:CMXCLCACCompletionBlock?) {
        if motionManager == nil {
            motionManager = CMMotionManager()
        }
        motionManager?.accelerometerUpdateInterval = AppCfg().sensorConfigurations.accelerometerUpdateInterval
        if motionManager!.isAccelerometerAvailable {
            motionManager?.startAccelerometerUpdates(to: OperationQueue.main, withHandler: {[weak self] (object, error) in guard let `self` = self else { return }
                self.motionManager?.accelerometerUpdateInterval = AppCfg().sensorConfigurations.accelerometerUpdateInterval
                if error != nil {
                    printInfoErrorIfRequired(error: error as NSError?)
                }else if  object != nil {
                    let accelerate = CMXAcceleration(rawData: object!, timeStamp: Date())
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        completion?(accelerate)
                    }
                }
                })
        }
    }
    
    func stopAccelerometerUpdates() {
        if motionManager != nil{
            if motionManager!.isAccelerometerActive {
                motionManager?.stopAccelerometerUpdates()
            }
        }
    }
    
    /// Gyrometer
    func startGyrometerUpdates(completion:CMXCLCRCompletionBlock?) {
        if motionManager == nil {
            motionManager = CMMotionManager()
        }
        motionManager?.gyroUpdateInterval = AppCfg().sensorConfigurations.gyroScopeUpdateInterval
        if motionManager!.isGyroAvailable {
            motionManager?.startGyroUpdates(to: OperationQueue.main, withHandler: {[weak self] (object, error) in guard let `self` = self else { return }
                self.motionManager?.gyroUpdateInterval = AppCfg().sensorConfigurations.gyroScopeUpdateInterval
                if error != nil {
                    printInfoErrorIfRequired(error: error as NSError?)
                }else if  object != nil {
                    let rotation = CMXRotation(rawData: object!.rotationRate, timeStamp: Date())
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        completion?(rotation)
                    }
                }
                })
        }
    }
    
    func stopGyrometerUpdates() {
        if motionManager != nil{
            if motionManager!.isGyroActive {
                motionManager?.stopGyroUpdates()
            }
        }
    }
    
    /// Magnetometer
    func startMagnetometerUpdates(completion:CMXCLCMFCompletionBlock?) {
        if motionManager == nil {
            motionManager = CMMotionManager()
        }
        //set motion interval
        motionManager?.magnetometerUpdateInterval = AppCfg().sensorConfigurations.magnetometerUpdateInterval
        if motionManager!.isMagnetometerAvailable {
            //start magneto meter readings.. Since Device motion update din't return megntic field data
            motionManager?.startMagnetometerUpdates(to: OperationQueue.main) {[weak self](magneticField, error) in guard let `self` = self else { return }
                self.motionManager?.magnetometerUpdateInterval = AppCfg().sensorConfigurations.magnetometerUpdateInterval
                if error != nil {
                    printInfoErrorIfRequired(error: error as NSError?)
                }else if magneticField != nil {
                    let magneticField = CMXMagneticField(rawData: magneticField!, timeStamp: Date())
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        completion?(magneticField)
                    }
                }
            }
        }
    }
    
    func stopMagnetometerUpdates() {
        if motionManager != nil{
            if motionManager!.isMagnetometerActive{
                motionManager?.stopMagnetometerUpdates()
            }
        }
    }
    
    /// Attitude
    func startAttitudeUpdates(completion:CMXCLCATCompletionBlock?) {
        if motionManager == nil {
            motionManager = CMMotionManager()
        }
        motionManager?.deviceMotionUpdateInterval = AppCfg().sensorConfigurations.deviceMotionUpdateInterval
        if motionManager!.isDeviceMotionAvailable {
            motionManager?.showsDeviceMovementDisplay = true
            motionManager?.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xArbitraryCorrectedZVertical)
            motionManager?.startDeviceMotionUpdates(to: OperationQueue.main) {[weak self] (object, error) in guard let `self` = self else { return }
                self.motionManager?.deviceMotionUpdateInterval = AppCfg().sensorConfigurations.deviceMotionUpdateInterval
                if error != nil {
                    printInfoErrorIfRequired(error: error as NSError?)
                }else if  object != nil {
                    let attitude = CMXAttitude(rawData: object!.attitude, timeStamp: Date())
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        completion?(attitude)
                    }
                }
            }
        }
    }
    
    func stopAttitudeUpdates() {
        if motionManager != nil{
            if motionManager!.isDeviceMotionActive {
                motionManager?.stopDeviceMotionUpdates()
            }
        }
    }
    
    /// Activity
    func startActivityUpdates(completion: CMXCLCMACompletionBlock?) {
        if motionActivityManager == nil {
            motionActivityManager = CMMotionActivityManager()
        }
        if CMMotionActivityManager.isActivityAvailable() {
            motionActivityManager?.startActivityUpdates(to: OperationQueue.main) {[weak self]
                activityData in guard let `self` = self else { return }
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    if activityData != nil{
                        completion?(CMXMotionActivity(motionActivity: activityData!))
                    }
                }
            }
        }
    }
    
    func stopActivityUpdates() {
        if motionActivityManager != nil{
            if CMMotionActivityManager.isActivityAvailable() {
                motionActivityManager?.stopActivityUpdates()
            }
        }
    }
    
    /// Pedometer
    func startPedometerUpdates(completion: CMXCLCPDCompletionBlock?) {
        pedometer = CMPedometer()
        pedometer?.startUpdates(from: Date() as Date) {[weak self]
            (pedometerData, error) in guard let `self` = self else { return }
            if error != nil {
                printInfo(data: "ERROR PEDOMETER UPDATES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
            } else {
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    if pedometerData != nil {
                        completion?(pedometerData!)
                    }
                }
            }
        }
    }
    
    func stopPedometerUpdates() {
        if pedometer != nil {
            pedometer?.stopUpdates()
            pedometer = nil
        }
    }
    
    /// Altitude
    func startAltitudeUpdates(completion: CMXCLCALCompletionBlock?) {
        altimeter = CMAltimeter()
        if CMAltimeter.isRelativeAltitudeAvailable(){
            altimeter?.startRelativeAltitudeUpdates(to: OperationQueue.main) {[weak self]
                (altitudeData, error) in guard let `self` = self else { return }
                if error != nil {
                    printInfo(data: "ERROR ALTIMETER UPDATES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
                } else {
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        if altitudeData != nil {
                            completion?(CMXAltitude(altitide: altitudeData!))
                        }
                    }
                }
            }
        }
    }
    
    func stopAltitudeUpdates() {
        if altimeter != nil {
            altimeter?.stopRelativeAltitudeUpdates()
            altimeter = nil
        }
    }
    
    func fetchHistoricalMotionActivities(completion: CMXCLCMASCompletionBlock?) {
        if motionActivityManager == nil {
            motionActivityManager = CMMotionActivityManager()
        }
        let oneWeekTimeInterval = 24 * 3600 as TimeInterval
        let fromDate = Date(timeIntervalSinceNow: -oneWeekTimeInterval)
        let toDate = Date()
        motionActivityManager?.queryActivityStarting(from: fromDate as Date,to: toDate as Date, to: OperationQueue.main) {[weak self]
            (activities, error) in guard let `self` = self else { return }
            if error != nil {
                printInfo(data: "ERROR QUERYING ACTIVITIES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
            }else{
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    var motionActivities = [CMXMotionActivity]()
                    if activities != nil {
                        for activity in activities!.reversed() {
                            motionActivities.append(CMXMotionActivity(motionActivity: activity))
                        }
                    }
                    completion?(motionActivities)
                }
            }
        }
    }
    
    func fetchHistoricalPedometerData(_ from:Date,to:Date,_ completion: CMXCLCPDCompletionBlock?) {
        pedometer?.queryPedometerData(from: from as Date, to:to as Date) {[weak self]
            (pedometerData, error) -> Void in guard let `self` = self else { return }
            if error != nil {
                printInfo(data: "ERROR QUERYING PEDOMETER DETAILS , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
            } else {
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    if pedometerData != nil {
                        completion?(pedometerData!)
                    }
                }
            }
        }
    }
}

