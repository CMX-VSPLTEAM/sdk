//
//  CMXMapCoordinate.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold map coordinate
public class CMXMapCoordinate: NSObject {

        /// <#Description#>
    private var rawX = 0.0
    /// <#Description#>
    private var rawY = 0.0
    /// <#Description#>
    private var rawZ = 0.0
    /// <#Description#>
    var rawUnit = "FEET"

    /// <#Description#>
    private var x = 0.0
    /// <#Description#>
    private var y = 0.0
    /// <#Description#>
    private var z = 0.0
    /// <#Description#>
    public var unit = "FEET"

    /// Description
    public var confidenceFactor = ""
    
    public var changeTime = 0.0

    /// <#Description#>
    ///
    /// - parameter changeTime: <#changeTime description#>
    /// - parameter x:          <#x description#>
    /// - parameter y:          <#y description#>
    /// - parameter z:          <#z description#>
    /// - parameter unit:       <#unit description#>
    /// - parameter rawX:       <#rawX description#>
    /// - parameter rawY:       <#rawY description#>
    /// - parameter rawZ:       <#rawZ description#>
    /// - parameter rawUnit:    <#rawUnit description#>
    ///
    /// - returns: <#return value description#>
    init(changeTime:Double, x:Double,y:Double,z:Double,unit:String,rawX:Double,rawY:Double,rawZ:Double,rawUnit:String) {
        self.changeTime=changeTime
        self.x = x
        self.y = y
        self.z = y
        self.rawX = x
        self.rawY = y
        self.rawZ = y
        self.unit = unit
        self.rawUnit = unit
    }
    
    public func xValue() -> Double {
        if !CMXCfg().userRawCoordinatesForCMXLocation {
            return self.x
        }else{
            if self.rawX != -999 {
                return self.rawX
            }else{
                return self.x
            }
        }
    }
    
    public func yValue() -> Double {
        if !CMXCfg().userRawCoordinatesForCMXLocation {
            return self.y
        }else{
            if self.rawY != -999 {
                return self.rawY
            }else{
                return self.y
            }
        }
    }
    
    public func zValue() -> Double {
        if !CMXCfg().userRawCoordinatesForCMXLocation {
            return self.z
        }else{
            if self.rawZ != -999 {
                return self.rawZ
            }else{
                return self.z
            }
        }
    }
    
    public func setXValue(value:Double) {
        self.x = value
        self.rawX = value
    }
    
    public func setYValue(value:Double) {
        self.y = value
        self.rawY = value
    }
    
    public func setZValue(value:Double) {
        self.z = value
        self.rawZ = value
    }
}
