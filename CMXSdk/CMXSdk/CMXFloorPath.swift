//
//  CMXNSObject.swift
//
//  Created by  on 19/10/16
//  Copyright (c) . All rights reserved.
//

import Foundation

public class CMXFloorPath: Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kCMXNSObjectBleMarkersKey: String = "bleMarkers"
	internal let kCMXNSObjectNodesKey: String = "nodes"
	internal let kCMXNSObjectVersionKey: String = "version"
    internal let kCMXNSObjectErrorRadiusKey: String = "error_radius"
    internal let kCMXNSObjectAngleFromTrueNorth: String = "angleFromTrueNorth"
	internal let kCMXNSObjectAngleOfXAxisToTrueNorthKey: String = "angleOfXAxisToTrueNorth"
	internal let kCMXNSObjectSegmentsKey: String = "segments"
	internal let kCMXNSObjectAesUidKey: String = "aesUid"
    internal let kCMXNSObjectAnglesKey: String = "angle"


    // MARK: Properties
	public var bleMarkers: [CMXBleMarkers]?
	public var nodes: [CMXNodes]?
    public var angles:[Double]?
	public var version: String?
	public var angleOfXAxisToTrueNorth: Float?
	public var segments: [CMXSegments]?
	public var aesUid: Int?
    public var error_radius: Float?
    public var angleFromTrueNorth: Float?



    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		bleMarkers = []
		if let items = json[kCMXNSObjectBleMarkersKey].array {
			for item in items {
				bleMarkers?.append(CMXBleMarkers(json: item))
			}
		} else {
			bleMarkers = nil
		}
		nodes = []
		if let items = json[kCMXNSObjectNodesKey].array {
			for item in items {
				nodes?.append(CMXNodes(json: item))
			}
		} else {
			nodes = nil
		}
        
        //parse angles
        angles = []
        if let items = json[kCMXNSObjectAnglesKey].array {
            for item in items {
                angles?.append(item.double!)
            }
        } else {
            angles = nil
        }
        
		version = json[kCMXNSObjectVersionKey].string
		angleOfXAxisToTrueNorth = json[kCMXNSObjectAngleOfXAxisToTrueNorthKey].float
		segments = []
		if let items = json[kCMXNSObjectSegmentsKey].array {
			for item in items {
				segments?.append(CMXSegments(json: item))
			}
		} else {
			segments = nil
		}
		aesUid = json[kCMXNSObjectAesUidKey].int
        error_radius = json[kCMXNSObjectErrorRadiusKey].float
        angleFromTrueNorth = json[kCMXNSObjectAngleFromTrueNorth].float

    }

    // MARK: ObjectMapper Initalizers
    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    required public init?(map: Map){

    }

    /**
    Map a JSON object to this class using ObjectMapper
    - parameter map: A mapping from ObjectMapper
    */
    public func mapping(map: Map) {
		bleMarkers <- map[kCMXNSObjectBleMarkersKey]
		nodes <- map[kCMXNSObjectNodesKey]
		version <- map[kCMXNSObjectVersionKey]
		angleOfXAxisToTrueNorth <- map[kCMXNSObjectAngleOfXAxisToTrueNorthKey]
		segments <- map[kCMXNSObjectSegmentsKey]
		aesUid <- map[kCMXNSObjectAesUidKey]
        error_radius <- map[kCMXNSObjectErrorRadiusKey]
        angleFromTrueNorth <- map[kCMXNSObjectAngleFromTrueNorth]
    }
}
