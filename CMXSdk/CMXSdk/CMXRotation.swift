//
//  CMXRotation.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion

/// Data modal to hold rotation
public class CMXRotation: CMXCapturableData {
    
    /// <#Description#>
    public var rawData : CMRotationRate!
    
    /// <#Description#>
    var timeStamp : Date!
    
    /// <#Description#>
    ///
    /// - parameter rawData: <#rawData description#>
    ///
    /// - returns: <#return value description#>
    init(rawData:CMRotationRate,timeStamp:Date) {
        self.rawData = rawData
        self.timeStamp = timeStamp
    }
}
