//
//  CMXBeaconsController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreLocation

public typealias CMXBCCompletionBlock = (_ beacons:CMXBeaconCollection) ->()
public typealias CMXBCFailureBlock = (_ error:NSError) ->()

/// Manager class dealling with beacons
class CMXBeaconsController: NSObject {
    static let BEACON_UUID: String = "e912b17f-0fdf-4f9a-a182-9f5561cf2a8c"
    
    
    let beaconRegion = CLBeaconRegion.init(proximityUUID: NSUUID.init(uuidString: BEACON_UUID)! as UUID, identifier: "CMX-iBeacon")
    /// initialize instance
    static let shared : CMXBeaconsController = {
        let instance = CMXBeaconsController()
        return instance
    }()
    
    var completionBlock : CMXBCCompletionBlock?
    var failureBlock : CMXBCFailureBlock?
    var request : BeaconRegionRequest?
    
    override init() {
    }
    
    func stopScanning() {
        request?.cancel()
        clearReferences()
    }
    
    func clearReferences(){
        self.completionBlock = nil
        self.failureBlock = nil
        self.request = nil
    }
    
    func scanForBeacons(completion:@escaping CMXBCCompletionBlock,failure:@escaping CMXBCFailureBlock) {
        self.completionBlock = completion
        let proximity = "e912b17f-0fdf-4f9a-a182-9f5561cf2a8c"
        do {
            let beacon = Beacon(proximity: proximity, major: nil, minor: nil)
            request = try Beacons.monitor(beacon: beacon, events: Event.Ranging, onStateDidChange: { state in
                
                }, onRangingBeacons: { visibleBeacons in
                    self.completionBlock?(CMXBeaconCollection(beacons: visibleBeacons,timeStamp:Date()))
                }, onError: { error in
                    printInfo(data: "scanForBeacons \(error.localizedDescription)")
            })
        } catch let error {
            printInfo(data: "scanForBeacons \(error.localizedDescription)")
        }
    }
}

