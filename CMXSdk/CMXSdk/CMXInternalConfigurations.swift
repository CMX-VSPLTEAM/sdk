//
//  CMXInternalConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Manager class to hold various runtime configurations
class CMXInternalConfigurations: NSObject {
    
    static let shared : CMXInternalConfigurations = {
        let instance = CMXInternalConfigurations()
        return instance
    }()
    
    override init() {
    }
    
    public var observationStartDate : NSDate?
    public var numberOfObservationsRequiredForFusionAlgorithm = 500
}

func Communication()->CMXCommunication {
    return CMXCommunication.shared
}

func Auth()->CMXAuthentication {
    return CMXAuthentication.shared
}

func Bc()->CMXBeaconsController {
    return CMXBeaconsController.shared
}

//func lmc()->CMXBeaconControllerLocation {
//    return CMXBeaconControllerLocation.shared
//}
//
func CMXLc()->CMXCMXLocationController {
    return CMXCMXLocationController.shared
}

func CMXCfg()->CMXConfigurations {
    return CMXConfigurations.shared
}

func Dec()->CMXDataExportController {
    return CMXDataExportController.shared
}

func Fusion()->CMXFusionController {
    return CMXFusionController.shared
}

func CfgInternal()->CMXInternalConfigurations {
    return CMXInternalConfigurations.shared
}

func Masc()->CMXMotionActivityServiceConfigurations {
    return CMXMotionActivityServiceConfigurations.shared
}

func Pdsc()->CMXPedometerDataServiceConfigurations {
    return CMXPedometerDataServiceConfigurations.shared
}

func CMX()->CMXSdk {
    return CMXSdk.shared
}

func SCfg()->CMXSensorConfigurations {
    return CMXSensorConfigurations.shared
}

func Sc()->CMXSensorController {
    return CMXSensorController.shared
}

func AppCfg()->CMXAppConfigurations {
    return CMXAppConfigurations.shared
}

func Hc()->CMXHeadingsController {
    return CMXHeadingsController.shared
}

func HCfg()->CMXHeadingConfigurations {
    return CMXHeadingConfigurations.shared
}

func MDCfg()->CMXMotionDetectionConfigurations {
    return CMXMotionDetectionConfigurations.shared
}
























