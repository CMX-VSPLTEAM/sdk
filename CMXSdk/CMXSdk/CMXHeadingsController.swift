//
//  CMXHeadingsController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreLocation

public typealias CMXHCCompletionBlock = (_ beacons:CMXHeadings) ->()
public typealias CMXHCFailureBlock = (_ error:NSError) ->()

/// Manager class dealling with beacons
class CMXHeadingsController: NSObject {
    
    /// initialize instance
    static let shared : CMXHeadingsController = {
        let instance = CMXHeadingsController()
        return instance
    }()
    
    var completionBlock : CMXHCCompletionBlock?
    var failureBlock : CMXHCFailureBlock?
    var request : Request?
    
    override init() {
        
    }
    
    func stopUpdates() {
        request?.cancel()
        clearReferences()
    }
    
    func clearReferences(){
        self.completionBlock = nil
        self.failureBlock = nil
        self.request = nil
    }
    
    func startUpdatingHeadings(completion:@escaping CMXHCCompletionBlock,failure:@escaping CMXHCFailureBlock) {
        request = Location.getHeading(HeadingFrequency.continuous(interval: HCfg().deviceHeadingUpdateInterval), accuracy: HCfg().accuracy, allowsCalibration: HCfg().allowsCalibration, didUpdate: { newHeading in
            self.completionBlock?(CMXHeadings(heading: newHeading, timeStamp: Date()))
            printInfo(data: "New Heading Observed \nAccuracy : \(newHeading.headingAccuracy)\nX : \(newHeading.x)\nY : \(newHeading.y)\nZ : \(newHeading.z)")
        }) { error in
            printInfo(data: "startUpdatingHeadings \(error.localizedDescription)")
        }
    }
}

