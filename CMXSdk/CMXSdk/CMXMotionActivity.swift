//
//  CMXMotionActivity.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */


import Foundation
import CoreMotion

/// Data modal to hold motionActivity
public class CMXMotionActivity: CMXCapturableData {
    
    /// <#Description#>
    public var motionActivity :CMMotionActivity!
    
    /// <#Description#>
    ///
    /// - parameter motionActivity: <#motionActivity description#>
    /// - parameter capturedAt:     <#capturedAt description#>
    ///
    /// - returns: <#return value description#>
    init(motionActivity:CMMotionActivity) {
        self.motionActivity = motionActivity
    }
}
