//
//  CMXBeaconInfo.swift
//  CMXSdk
//
//  Created by abhishmu on 11/1/16.
//  Copyright © 2016 Cisco. All rights reserved.
//

import Foundation
import CoreLocation

public class CMXBeaconInfo {
    
    /// <#Description#>
    var minorID : Int
    var majorID : Int
    var proximity : Int
    var accuracy : Double
    var rssi : Int
   
    /// <#Description#>
    ///
    /// - parameter minor: <#minor description#>
    /// - parameter major: <#major description#>
    /// - parameter prox:  <#prox description#>
    /// - parameter accu:  <#accu description#>
    /// - parameter rssi:  <#rssi description#>
    ///
    /// - returns: <#return value description#>
    init(minor: Int, major: Int, prox: Int, accu: Double, rssi: Int) {
        self.minorID = minor
        self.majorID = major
        self.proximity = prox
        self.accuracy = accu
        self.rssi = rssi
    }
    
    func getMinorID()-> Int{
        return minorID
    }
    
    func getProximity() -> Int {
        return self.proximity
    }
    
    func getAccuracy() -> Double {
        return self.accuracy
    }
    
    func getRSSI() -> Int {
        return self.rssi
    }
}
