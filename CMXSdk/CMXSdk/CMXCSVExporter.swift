//
//
//  CMXCSVExporter.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import UIKit
import Foundation

class CMXCSVExporter: NSObject {
    
    /// <#Description#>
    ///
    /// - parameter fileName:      <#fileName description#>
    /// - parameter keyArray:      <#keyArray description#>
    /// - parameter variableArray: <#variableArray description#>
    ///
    /// - returns: <#return value description#>
    class func exportToCsv(_ fileName : NSString,_ folderName : NSString, keyArray:NSArray, variableArray:NSArray)->String{
        let content: NSString =  "\(keyArray.componentsJoined(by: ",") as NSString)\n" as NSString
        let stringObject : NSMutableString? = NSMutableString()
        let paths: NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray;
        let documentsDirectory : NSString = paths[0] as! NSString
        let filePathToWrite = "\(documentsDirectory)/\(folderName)/\(fileName)"
        stringObject?.append("\(content)")
        let b : Int = (variableArray[0] as AnyObject).count as Int
        for j in 0..<b {
            let stringValue : NSMutableString = NSMutableString()
            let varArrayCount : Int = variableArray.count as Int
            for k in 0..<varArrayCount{
                let newVar = variableArray[k] as! NSArray
                stringValue.append("\(newVar[j]),")
            }
            let original : NSString  = "\(stringValue)" as NSString
            let locationInt : NSRange = original.range(of: ",", options: NSString.CompareOptions.backwards)
            var newStr : NSString = ""
            if(locationInt.location != NSNotFound){
                newStr = "\(stringValue)" as NSString
                newStr = newStr.replacingCharacters(in: locationInt, with: "\n") as NSString
            }
            stringObject?.append(newStr as String)
        }
        do{
            try FileManager.default.createDirectory(at: URL(fileURLWithPath: "\(documentsDirectory)/\(folderName)"), withIntermediateDirectories: true, attributes: nil)
            try stringObject?.write(toFile: filePathToWrite, atomically: false, encoding: String.Encoding.utf8.rawValue)
        }catch{
            printInfo(data:error)
        }
        return filePathToWrite
    }
}
