//
//  CMXActivity.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

public enum CMXActivityType {
    case Unknown
    case Stationary
    case Walking
    case Running
    case Automotive
    case Cycling
}

/// <#Description#>
public class CMXActivity: CMXCapturableData {
    
    /// <#Description#>
    var activityType : CMXActivityType!
    
    /// <#Description#>
    var timeStamp : Date!
    
    /// <#Description#>
    ///
    /// - parameter activityType: <#activityType description#>
    /// - parameter timeStamp:    <#timeStamp description#>
    ///
    /// - returns: <#return value description#>
    init(activityType:CMXActivityType,timeStamp:Date) {
        self.activityType = activityType
        self.timeStamp = timeStamp
    }
}
