//
//  CMXHeadingConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

class CMXHeadingConfigurations: NSObject {

    /// initialize instance
    static let shared : CMXHeadingConfigurations = {
        let instance = CMXHeadingConfigurations()
        return instance
    }()
    
    var deviceHeadingUpdateInterval = 0.1
    var accuracy = 1.5
    var allowsCalibration = true
    
    override init() {
    }
}
