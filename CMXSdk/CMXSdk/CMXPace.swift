//
//  CMXPace.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation


/// <#Description#>
public class CMXPace: CMXCapturableData {
    
    /// <#Description#>
    var pace : Double!
    
    /// <#Description#>
    var timeStamp : Date!
    
    /// <#Description#>
    ///
    /// - parameter pace:      <#pace description#>
    /// - parameter timeStamp: <#timeStamp description#>
    ///
    /// - returns: <#return value description#>
    init(pace:Double,timeStamp:Date) {
        self.pace = pace
        self.timeStamp = timeStamp
    }
}
