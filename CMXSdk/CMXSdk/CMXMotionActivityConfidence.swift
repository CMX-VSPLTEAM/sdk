//
//  CMXMotionActivityConfidence.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion

public class CMXMotionActivityConfidence: CMXCapturableData {
    
    /// <#Description#>
    var confidence : CMMotionActivityConfidence!
    
    /// <#Description#>
    var timeStamp : Date!
    
    init(confidence:CMMotionActivityConfidence,timeStamp:Date) {
        self.confidence = confidence
        self.timeStamp = timeStamp
    }
}
