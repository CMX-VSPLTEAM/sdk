//
//  CMXMotion.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation


public enum CMXMotionState {
    case Stationary
    case Moving
}

/// <#Description#>
public class CMXMotion {
    
    
    /// <#Description#>
    var motionState = CMXMotionState.Stationary
    
    
    /// <#Description#>
    var originalCadence : Double!
    
    /// <#Description#>
    var computedCadence : Double!
    
    /// <#Description#>
    var pace : Double!
    
    /// <#Description#>
    var timeStamp : Date!
    
    /// <#Description#>
    ///
    /// - parameter motionState: <#motionState description#>
    /// - parameter timeStamp:   <#timeStamp description#>
    ///
    /// - returns: <#return value description#>
    init(motionState:CMXMotionState,originalCadence:Double, pace:Double, timeStamp:Date) {
        self.originalCadence = originalCadence
        self.pace = pace
        self.motionState = motionState
        self.timeStamp = timeStamp
    }
    
    func getCadence() -> Double {
        if MDCfg().useComputedCadence {
            return computedCadence
        }else{
            return originalCadence
        }
    }
    
}
