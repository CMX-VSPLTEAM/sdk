//
//  CMXLocationData.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold location on map
public class CMXLocationData: CMXCapturableData {
    
    /// <#Description#>
    public var mapInfo : CMXMapInfo

    /// <#Description#>
    public var mapCoordinate : CMXMapCoordinate
    
    /// <#Description#>
    var timeStamp : Date!
    
    /// device state when CMX update received
    var deviceActivity: String!

    /// <#Description#>
    ///
    /// - parameter mapInfo:       <#mapInfo description#>
    /// - parameter mapCoordinate: <#mapCoordinate description#>
    ///
    /// - returns: <#return value description#>
    init(mapInfo:CMXMapInfo,mapCoordinate:CMXMapCoordinate,timeStamp:Date) {
        self.mapInfo = mapInfo
        self.mapCoordinate = mapCoordinate
        self.timeStamp = timeStamp
    }
}

