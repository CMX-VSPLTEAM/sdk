//
//  CMXPedometerDataServiceConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

class CMXPedometerDataServiceConfigurations: NSObject {
    /// initialize instance
    static let shared : CMXPedometerDataServiceConfigurations = {
        let instance = CMXPedometerDataServiceConfigurations()
        return instance
    }()
   
    override init() {
    }

    var furtherRefineCadenceValues = false
    
    var pedometerStaleTimeThreshold = 5000.0 //in milliseconds
    

    
    var refineCadenceUsingAveragingUsingLastTwoObservations = true
    
    //Note:- if refineCadenceUsingAveragingUsingLastTwoObservations is true ,
    //       then setting refineCadenceUsingMovingAveragingLastFewSecondsObservations
    //       to true will not make any effect
    var refineCadenceUsingMovingAveragingLastFewSecondsObservations = false

    var movingAverageTimeThreshold = 5.0

}
