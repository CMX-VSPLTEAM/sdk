//
//  CMXBeaconCollectionFormatted.swift
//  CMXSdk
//
//  Created by abhishmu on 11/1/16.
//  Copyright © 2016 Cisco. All rights reserved.
//

import Foundation

import CoreLocation

/// Data modal to beacon heard in BLEMarkers format
public class CMXBeaconCollectionFormatted: CMXCapturableData {
    
    /// <#Description#>
    let beaconsInfo : [CMXBeaconInfo]?
    
    /// <#Description#>
    var timeStamp : Date!
    
    /// <#Description#>
    ///
    /// - parameter beacon: <#beacon description#>
    ///
    /// - returns: <#return value description#>
    init(beaconsInfo:[CMXBeaconInfo]?,timeStamp:Date) {
        self.beaconsInfo = beaconsInfo
        self.timeStamp = timeStamp
    }
    
    func getTimestamp()->Date{
        return timeStamp!
    }

}
