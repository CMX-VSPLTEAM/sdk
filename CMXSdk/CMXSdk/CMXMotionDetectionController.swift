//
//  CMXMotionDetectionController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

// Content Description : This class reads sensers and process data and decides wether the user is stationary or moving

import Foundation
import CoreMotion

class CMXMotionDetectionController: NSObject {

    let motionActivityService = CMXMotionActivityService()
    let pedometerDataService = CMXPedometerDataService()
    
    var detectedMotionStateArray = [CMXMotion]()

    func startProcessing() {
        motionActivityService.startProcessing()
        pedometerDataService.startProcessing()
    }
    
    func getUserMotion() -> CMXMotion {

        /*
         *
         **************************************      SUGGESTED ALOGORITHM       ***********************
         *
         *  Objective : COMBINE RESULTS FROM MOTION ACTIVITY MANAGER AND PEDOMETER
         *
         **********************************************************************************************
         *
         *  1. copy current predicted value from Pedometer
         *  MAMPdrLabel[i] <- PdrLabel[i]
         *
         *  2. apply the next logic
         *  if (!is.na(MAMLabel[i])) {
         *      if (is.na(MAMLabel[i-1]) || MAMLabel[i-1] != MAMLabel[i]) {
         *          if (MAMLabel[i] != PdrLabel[i]) {
         *              MAMPdrLabel[i] <- MAMLabel[i]
         *          }
         *      }
         *  }
         *
         *   // walking
         *   if (!is.na(Cadence[i]) && Cadence[i] > 0 && !is.na(MAMLabel[i]) && MAMLabel[i] == 1) {
         *       MAMPdrLabel[i] <- 1
         *   }
         *   //stationary
         *   if (!is.na(MAMLabel[i]) && MAMLabel[i] == 0 && MAMConfidence[i] == "high") {
         *       MAMPdrLabel[i] <- 0
         *   }
         *
         **********************************************************************************************
         *
         */
        
        let pedometerStateArray = pedometerDataService.pedometerStateArray
       // var activityMotionStateArray = motionActivityService.activityMotionStateArray
        
        let currentTime = Date()
        let timeThreshold = 5000.0 // 5 seconds
        
        if pedometerStateArray.last != nil {// && activityMotionStateArray.last != nil{
            
            
            let pedometerData = pedometerStateArray.last!
            
            printInfo(data: "currentTime: \(currentTime.timeIntervalInMilliSecondsSince1970()), pedometerTime: \(pedometerData.timeStamp.timeIntervalInMilliSecondsSince1970())")
          
            printInfo(data: "timeDiff: \((currentTime.timeIntervalInMilliSecondsSince1970())-(pedometerData.timeStamp.timeIntervalInMilliSecondsSince1970()))")
            
            //printInfo(data:currentTime)
            if ((currentTime.timeIntervalInMilliSecondsSince1970()) - (pedometerData.timeStamp.timeIntervalInMilliSecondsSince1970()) <= CMXMotionDetectionConfigurations.shared.motionStaleTimeThreshold) {
                
            printInfo(data: "**************************adding pedometer MOVING to motiondetection**************___________")
            detectedMotionStateArray.append(CMXMotion(motionState: pedometerData.motionState, originalCadence: pedometerData.originalCadence, pace: pedometerData.pace, timeStamp: currentTime))
                
              //detectedMotionStateArray.append(pedometerStateArray.last!)
            }
            else{
                printInfo(data: "**********TOO OLD PEDOMETERDATA************")
                printInfo(data: "*******************adding STATIONARY to motiondetection**************___________")
                

                detectedMotionStateArray.append(CMXMotion(motionState: .Stationary, originalCadence: 0.0, pace: 0.0, timeStamp: Date()))
                pedometerDataService.pedometerStateArray.removeAll()
                
            }
        
            
//            if activityMotionStateArray.last != nil {
//                if ((activityMotionStateArray.count <= 1) || (activityMotionStateArray[activityMotionStateArray.count - 2] != activityMotionStateArray[activityMotionStateArray.count - 1])) {
//                    if activityMotionStateArray.last != pedometerStateArray.last {
//                        detectedMotionStateArray[detectedMotionStateArray.count - 1] = activityMotionStateArray.last!
//                    }
//                }
//            }
            
        }
       
        if detectedMotionStateArray.count == 0 {
            printInfo(data: "**********NO PEDOMETERDATA************")
            printInfo(data: "*******************adding STATIONARY to motiondetection**************___________")
            
            detectedMotionStateArray.append((CMXMotion(motionState: .Stationary, originalCadence: 0.0 , pace:  0.0 , timeStamp: Date())))
            pedometerDataService.pedometerStateArray.removeAll()

        }
        
        return detectedMotionStateArray.last!
     }
}
