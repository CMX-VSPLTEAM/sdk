//
//  CMXHelper.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */


// Content Description : This file contains helper functions for general usage

import Foundation

extension Date {
    public func cmxhfToStringCustom(dateStyle: DateFormatter.Style = .medium, timeStyle: DateFormatter.Style = .medium) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = dateStyle
        formatter.timeStyle = timeStyle
        return formatter.string(from: self).replacingOccurrences(of: ",", with: " ")
    }
    
    public func timeIntervalInMilliSecondsSince1970() -> TimeInterval {
        return timeIntervalSince1970*1000
    }
    
    public func timeIntervalInSecondsSince1970() -> TimeInterval {
        return timeIntervalSince1970
    }
}

public func cmxhfDocumentsDirectory() -> String {
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask, true)[0]
    return documentsPath
}

extension String {
    var cmxhfLength: Int {
        return self.characters.count
    }
    func cmxhfIsNumber() -> Bool {
        if let _ = NumberFormatter().number(from: self) {
            return true
        }
        return false
    }
    func cmxhfToInt() -> Int? {
        if let num = NumberFormatter().number(from: self) {
            return num.intValue
        } else {
            return nil
        }
    }
     func cmxhfToDouble() -> Double? {
        if let num = NumberFormatter().number(from: self) {
            return num.doubleValue
        } else {
            return nil
        }
    }
     func cmxhfToFloat() -> Float? {
        if let num = NumberFormatter().number(from: self) {
            return num.floatValue
        } else {
            return nil
        }
    }
}

/// <#Description#>
///
/// - parameter sourceDictionary:      <#sourceDictionary description#>
/// - parameter sourceKey:             <#sourceKey description#>
/// - parameter destinationDictionary: <#destinationDictionary description#>
/// - parameter destinationKey:        <#destinationKey description#>
/// - parameter methodName:            <#methodName description#>
/// - parameter asString:              <#asString description#>
 func cmxhfCopyData(_ sourceDictionary:NSDictionary?,sourceKey:NSString?,destinationDictionary:NSDictionary?,destinationKey:NSString?,methodName:NSString?,asString:Bool = false) {
    if sourceDictionary != nil && sourceKey != nil && destinationDictionary != nil && destinationKey != nil && sourceDictionary!.object(forKey: sourceKey! as String) != nil {
        if asString {
            let value = "\(sourceDictionary!.object(forKey: (sourceKey as! String))!)"
            destinationDictionary?.setValue(value, forKey:destinationKey as! String)
        }else{
            let value = sourceDictionary!.object(forKey: (sourceKey as! String))!
            destinationDictionary?.setValue(value, forKey:destinationKey as! String)
        }
    }else{
        #if DEBUG
            cmxhfReportMissingParameter(sourceKey!, methodName: methodName!)
        #endif
    }
}

/// <#Description#>
///
/// - parameter data:                  <#data description#>
/// - parameter destinationDictionary: <#destinationDictionary description#>
/// - parameter destinationKey:        <#destinationKey description#>
/// - parameter methodName:            <#methodName description#>
 func cmxhfCopyData(_ data:AnyObject?,destinationDictionary:NSDictionary?,destinationKey:NSString?,methodName:NSString?) {
    if data != nil && destinationDictionary != nil && destinationKey != nil{
        destinationDictionary?.setValue(data!, forKey: (destinationKey as! String))
    }else{
        #if DEBUG
            cmxhfReportMissingParameter(destinationKey!, methodName: methodName!)
        #endif
    }
}

/// <#Description#>
///
/// - parameter error:      <#error description#>
/// - parameter methodName: <#methodName description#>
 func cmxhfPrintErrorMessage (_ error : NSError? , methodName : NSString?) -> () {
    printInfo(data: "\nERROR MESSAGE :--- \(error?.localizedDescription) ---IN METHOD : \(methodName)\n")
}

/// <#Description#>
///
/// - parameter missingParameter: <#missingParameter description#>
/// - parameter methodName:       <#methodName description#>
 func cmxhfReportMissingParameter (_ missingParameter : NSString , methodName : NSString) -> () {
    printInfo(data: "\nMISSING PARAMETER :--- \(missingParameter) ---IN METHOD : \(methodName)\n")
}

/// <#Description#>
///
/// - parameter data:       <#data description#>
/// - parameter methodName: <#methodName description#>
///
/// - returns: <#return value description#>
 func cmxhfParsedJson (_ data : Data?,methodName: NSString) -> (AnyObject?) {
    let parsedData : Any?
    do {
        parsedData =  try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
    } catch {
        parsedData = nil
    }
    return parsedData as (AnyObject?)
}

/// <#Description#>
///
/// - parameter data:       <#data description#>
/// - parameter methodName: <#methodName description#>
///
/// - returns: <#return value description#>
 func cmxhfParsedJsonFrom (_ data : Data?,methodName: NSString) -> (AnyObject?) {
    let parsedData : Any?
    do {
        parsedData =  try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
    } catch {
        parsedData = nil;
    }
    #if DEBUG
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.low).async {
            let dataAsString : NSString? = NSString(data: data!,encoding: String.Encoding.utf8.rawValue);
            if parsedData != nil {
//                printInfo(data: "\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(methodName)\n\n\(dataAsString!)\n\n\n")
//                printInfo(data: "\n\nRECEIVED DATA AFTER PARSING IS \n\n\(methodName)\n\n\(parsedData!)\n\n\n")
            }else{
                if dataAsString != nil {
//                    printInfo(data: "\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(methodName)\n\n\(dataAsString!)\n\n\n")
                }else{
//                    printInfo(data: "\n\nRECEIVED DATA BEFORE PARSING IS \n\n\(methodName)\n\n\(dataAsString)\n\n\n")
                }
//                printInfo(data: "\n\nRECEIVED DATA AFTER PARSING IS \n\n\(methodName)\n\n\(parsedData)\n\n\n")
            }
        }
    #endif
    return parsedData as (AnyObject?)
}

/// <#Description#>
///
/// - parameter object: <#object description#>
///
/// - returns: <#return value description#>
func cmxhfIsNull(_ object:AnyObject?)->(Bool){
    if object != nil{
        if object! is NSNull {
            return true
        }else if object! is NSString {
            if object as! String == "<null>" || object as! String == "" || object as! String == "null" {
                return true
            }
        }
        return false
    }
    return true
}

/// <#Description#>
///
/// - parameter object: <#object description#>
///
/// - returns: <#return value description#>
func cmxhfIsNotNull(_ object:AnyObject?)->(Bool){
    return !cmxhfIsNull(object)
}

/// <#Description#>
///
/// - parameter object:    <#object description#>
/// - parameter alternate: <#alternate description#>
///
/// - returns: <#return value description#>
func cmxhfSafeString(_ object:Any?,alternate:String="")->String{
    if let stringValue = object as? String{
        return stringValue
    }else if cmxhfIsNotNull(object as AnyObject?){
        return "\(object!)"
    }
    return alternate
}

/// <#Description#>
///
/// - parameter object:    <#object description#>
/// - parameter alternate: <#alternate description#>
///
/// - returns: <#return value description#>
func cmxhfSafeNSString(_ object:Any?,alternate:String="")->NSString{
    return cmxhfSafeString(object, alternate: alternate) as NSString
}

/// <#Description#>
///
/// - parameter object:    <#object description#>
/// - parameter alternate: <#alternate description#>
///
/// - returns: <#return value description#>
func cmxhfsafeBool(_ object:Any?,alternate:Bool=false)->Bool{
    if let numberValue = object as? NSNumber{
        return numberValue.boolValue
    }else if let boolValue = object as? Bool{
        return boolValue
    }else if let stringValue = object as? String  , stringValue.cmxhfIsNumber() {
        return stringValue == "1"
    }else if let intValue = object as? Int32{
        return NSNumber(value:intValue).boolValue
    }else if let doubleValue = object as? Double{
        return NSNumber(value:doubleValue).boolValue
    }
    return alternate
}

/// <#Description#>
///
/// - parameter object:    <#object description#>
/// - parameter alternate: <#alternate description#>
///
/// - returns: <#return value description#>
func cmxhfSafeInt(_ object:Any?,alternate:Int=0)->Int{
    if let intValue = object as? Int{
        return intValue
    }else if let numberValue = object as? NSNumber{
        return numberValue.intValue
    }else if let boolValue = object as? Bool{
        return boolValue ? 1 : 0
    }else if let stringValue = object as? String  , stringValue.cmxhfIsNumber() {
        return stringValue.cmxhfToInt()!
    }else if let doubleValue = object as? Double{
        return Int(doubleValue)
    }
    return alternate
}

/// <#Description#>
///
/// - parameter object:    <#object description#>
/// - parameter alternate: <#alternate description#>
///
/// - returns: <#return value description#>
func cmxhfSafeDouble(_ object:Any?,alternate:Double=0.0)->Double{
    if let numberValue = object as? NSNumber{
        return numberValue.doubleValue
    }else if let stringValue = object as? String  , stringValue.cmxhfIsNumber() {
        return Double(stringValue.cmxhfToInt()!)
    }else if let doubleValue = object as? Double{
        return doubleValue
    }
    return alternate
}

func printInfoErrorIfRequired(error:Error?){
    if AppCfg().loggingEnabled {
        if cmxhfIsNotNull(error as AnyObject?){
            printInfo(data:"\(error!.localizedDescription)")
            CMXFusionController().xcgLogger.error(error);
        }else{
            printInfo(data:"UnKnown error")
        }
    }
}

private func printMessage(data:String?){
    if data != nil {
        print(data!)
    }
}

func printInfo(data:Any){
    if AppCfg().loggingEnabled && AppCfg().logInfoMessages{
        CMXFusionController().xcgLogger.info(data);
        printMessage(data:"\(Date()) INFORMATION : \(data)\n")
    }
}

func printObservation(data:Any){
    if AppCfg().loggingEnabled && AppCfg().logObservationMessages {
        CMXFusionController().xcgLogger.info(data);
        printMessage(data:"\(Date()) OBSERVATION : \(data)\n")
    }
}

func printSuccess(data:Any){
    if AppCfg().loggingEnabled && AppCfg().logSuccessMessages {
        CMXFusionController().xcgLogger.info(data);
        printMessage(data:"\(Date()) SUCCESS : \(data)\n")
    }
}

func printWarning(data:Any){
    if AppCfg().loggingEnabled && AppCfg().logWarningMessages {
        CMXFusionController().xcgLogger.warning(data);
        printMessage(data:"\(Date()) WARNING : \(data)\n")
    }
}

func printError(data:Any){
    if AppCfg().loggingEnabled && AppCfg().logErrorMessages {
        CMXFusionController().xcgLogger.error(data);
        printMessage(data:"\(Date()) ERROR : \(data)\n")
    }
}

