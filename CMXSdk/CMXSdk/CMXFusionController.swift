//
//  CMXFusionController.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */


// Content Description : This class deals with anything related to CMX Server

import Foundation
import CoreGraphics
import CoreLocation
import XCGLogger


/// Call back handlers , block based
public typealias CMXFSuccessUpdateBlock = (_ update:CMXLocationData) ->()
public typealias CMXFFailureUpdateBlock = (_ error:NSError?) ->()

/// Manager class dealing with user location update , it usually combines bvar motion sensors observations and location from CMX server , and after performing some data optimisation algorithm return most accurate location data.

class CMXFusionController: NSObject {
    
    //Basic Functionallity
    static let shared : CMXFusionController = {
        let instance = CMXFusionController()
        return instance
    }()
    
    override init() {
    }
    
    var locationDataToApp : CMXLocationData?
    var updateLocation : CMXFSuccessUpdateBlock?
    var failureUpdateLocation : CMXFFailureUpdateBlock?
    var macAddress = ""
    var updateInterval = 1.0
    
    
    //Fusion
    var lastMotion = CMXMotion(motionState: .Stationary, originalCadence: 0.0, pace: 0.0, timeStamp: Date())
    var currentMotion = CMXMotion(motionState: .Stationary, originalCadence: 0.0, pace: 0.0, timeStamp: Date())
    var aggregatedMovingList = [CMXLocationData]()
    var aggregatedStationaryList = [CMXLocationData]()
    var currentLocationDataToSnapToPath : CMXLocationData?
    var previousLocationDataToSnapToPath : CMXLocationData?
    var previousCMXLocation : CMXLocationData?
    var currentCMXLocation : CMXLocationData?
    
    
    //Motion Detection
    let motionDetectionController = CMXMotionDetectionController()
    
    //CMX Location
    var lastCMXLocationValue:CMXLocationData?
    var lastMacAddress:String?
    var cmxLocationsValues = [CMXLocationData]()
    var ableToGetLocationUpdate = true
    
    
    //Near By Beacons
    var nearByBeacons = [CMXBeaconCollection]()
    var nearByBeaconsFormatted = [CMXBeaconCollectionFormatted]()
    
    //Device Headings
    var headings = [CMXHeadings]()
    
    //Path Matching
    var floorPath : CMXFloorPath?
    
    let xcgLogger = XCGLogger.default
}




// MARK: - Basic Functionallity
extension CMXFusionController {
    
    /// <#Description#>
    public func logOut() {
        stopLocationUpdates()
        clearReferences()
        CMXLc().logOut()
        Sc().logOut()
    }
    
    /// <#Description#>
    func clearReferences() {
        self.updateLocation = nil
        self.failureUpdateLocation = nil
        self.cmxLocationsValues.removeAll()
        self.nearByBeacons.removeAll()
        self.nearByBeaconsFormatted.removeAll()
        self.cmxLocationsValues.removeAll()
        lastMotion = CMXMotion(motionState: .Stationary, originalCadence: 0.0, pace: 0.0, timeStamp: Date())
        currentMotion = CMXMotion(motionState: .Stationary, originalCadence: 0.0, pace: 0.0, timeStamp: Date())
        aggregatedMovingList = [CMXLocationData]()
        aggregatedStationaryList = [CMXLocationData]()
        currentLocationDataToSnapToPath = nil
        previousLocationDataToSnapToPath = nil
        previousCMXLocation = nil
        currentCMXLocation = nil
        lastCMXLocationValue = nil
        lastMacAddress = nil
        cmxLocationsValues = [CMXLocationData]()
        ableToGetLocationUpdate = false
        nearByBeacons = [CMXBeaconCollection]()
        nearByBeaconsFormatted = [CMXBeaconCollectionFormatted]()
        floorPath = nil
    }
    
    
    func intializeLogger() {
        
        let timeInterval = Date().timeIntervalInMilliSecondsSince1970();
        let fileName = "\(timeInterval)" + ".log";
        
        let paths: NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray;
        let documentsDirectory : NSString = paths[0] as! NSString
        let filePathToWrite = "\(documentsDirectory)/\(fileName)"
        
        xcgLogger.setup(level: .debug, showThreadName: false, showLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: filePathToWrite, fileLevel: .debug)
    }
    
    /// <#Description#>
    func loadFloorPath() {
        Communication().getMapFloorPathFromCMX(["":""]) { (response) in
            if response is String {
                let jsonString = response as! String
                self.floorPath = CMXFloorPath(JSONString: jsonString)
                printInfo(data: "FLOOR PATH")
                printInfo(data: self.floorPath)
            }
        }
    }
    
    /// <#Description#>
    func stopLocationUpdates() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        exportCollectedDataToFile()
        CMXLc().stopCMXLocationUpdates()
        Sc().stopAllUpdates()
        clearReferences()

    }
    
    /// <#Description#>
    func fetchLocationAndUpdate(){
        intializeLogger()
        startCMXLocationUpdates()
        startMotionDetectionUpdates()
        startNearByBeaconUpdates()
        startHeadingUpdates()
        applyFusionAndUpdate()
        loadFloorPath()
    }
    
    
    /// <#Description#>
    func exportCollectedDataToFile() {
        if AppCfg().writeDataToCSVFile {
            var folderName = ""
            for i in 1...1024 {
                let path = "\(cmxhfDocumentsDirectory())/\(i)"
                if FileManager.default.fileExists(atPath: path, isDirectory: nil){
                    continue
                }else{
                    folderName = "\(i)"
                    break
                }
            }
            Dec().exportCMXLocationValues(collection: cmxLocationsValues,folderName: folderName)
            Dec().exportBeaconsValues(collection: nearByBeacons,folderName: folderName)
        }
    }
}


// MARK: - Initiate
extension CMXFusionController {
    
    /// <#Description#>
    ///
    /// - parameter macAddress:     <#macAddress description#>
    /// - parameter updateInterval: <#updateInterval description#>
    /// - parameter update:         <#update description#>
    /// - parameter failure:        <#failure description#>
    func startLocationUpdates(_ macAddress:String,_ updateInterval: Double = 0.2,_ update:@escaping CMXFSuccessUpdateBlock,failure:@escaping CMXFFailureUpdateBlock) {
        if Auth().isLoggedIn {
            AppCfg().userLocationCallBackUpdateInterval = updateInterval
            self.macAddress = macAddress
            self.updateLocation = update
            self.failureUpdateLocation = failure
            self.updateInterval = updateInterval
            CfgInternal().observationStartDate = NSDate()
            ableToGetLocationUpdate = true
            
            /// <#Description#>
            func reScheduleUpdate(){
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXFusionController.fetchLocationAndUpdate), object: nil)
                self.perform(#selector(CMXFusionController.fetchLocationAndUpdate), with: nil, afterDelay: TimeInterval(AppCfg().userLocationCallBackUpdateInterval))
            }
            
            reScheduleUpdate()
        }else{
            failure(NSError(domain: "Invalid", code: 0, userInfo: ["description":"please login first"]))
        }
    }
    
}


// MARK: - Fusion
extension CMXFusionController {
    
    func applyFusionAndUpdate() {
        
        func updateFailure(){
            let enableUpdatingFailuresAlso = false
            if enableUpdatingFailuresAlso {
                self.failureUpdateLocation?(NSError(domain: "Invalid", code: 0, userInfo: ["description":"device can not be located at this moment"]))
            }
        }
        
        func canContinueToFuse()->Bool {
            if ableToGetLocationUpdate {
                if lastCMXLocationValue == nil {
                    lastCMXLocationValue = cmxLocationsValues.last
                    if lastCMXLocationValue != nil {
                        return true
                    }else{
                        return false
                    }
                }else{
                    return true
                }
            }
            return false
        }
        
        func scheduleNextUpdate(){
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXFusionController.applyFusionAndUpdate), object: nil)
            self.perform(#selector(CMXFusionController.applyFusionAndUpdate), with: nil, afterDelay: updateInterval)
        }
        
        if canContinueToFuse(){
            
            var useNearByBeaconsOnlyToFuseLocation = false
            var newMapLocationData:CMXLocationData? = nil
            let simplyReturnLatestLocationUpdateFromCMX = false
            
            if self.nearByBeacons.count > 0 && self.floorPath != nil && self.floorPath!.bleMarkers!.count > 0 {
                printObservation(data:"ENTERED INTO BEACON LOGIC")
                newMapLocationData = getLocationFromNearByBeacons_v1(validMarkers: self.floorPath!.bleMarkers!)
                let currCMXLoc = cmxLocationsValues.last!
                if newMapLocationData?.mapCoordinate.xValue() != currCMXLoc.mapCoordinate.xValue() && newMapLocationData?.mapCoordinate.yValue() != currCMXLoc.mapCoordinate.yValue(){
                    useNearByBeaconsOnlyToFuseLocation = true
                    printWarning(data: "useNearByBeaconsOnlyToFuseLocation = true")
                }else{
                    printWarning(data: "useNearByBeaconsOnlyToFuseLocation = false")
                }
            }
            else{
                printError(data:"BEACONS DATA NOT AVAILABLE")
            }
            if !useNearByBeaconsOnlyToFuseLocation {
                if simplyReturnLatestLocationUpdateFromCMX {
                    
                    printObservation(data:"simplyReturnLatestLocationUpdateFromCMX = true")
                    
                    self.updateLocation?(cmxLocationsValues.last!)
                    
                    printSuccess(data: "latest cmx value (\(cmxLocationsValues.last?.mapCoordinate.xValue()),\(cmxLocationsValues.last?.mapCoordinate.yValue()))updated to app")
                
                }
                else{//motion detection logic
                
                    printObservation(data:"WILL APPLY MOTION DETECTION")
                    
                    newMapLocationData = cmxLocationsValues.last!
                    
                    currentMotion = motionDetectionController.getUserMotion()
                    
                    printObservation(data:"DETECTED MOTION : \(currentMotion.motionState)")

                    if (currentMotion.pace != nil){
                        printInfo(data:currentMotion.pace)
                    }
                    if lastMotion.motionState != currentMotion.motionState {
                        printObservation(data:"APPLYING TRANSITION LOGIC")
                        
                        newMapLocationData = processTransition(previousMotion: lastMotion, currentMotion: currentMotion)
                    }
                    else{
                        
                        if(currentMotion.motionState == .Stationary){
                            printObservation(data:"STATIONARY LOGIC")
                            
                            aggregatedStationaryList.append(lastCMXLocationValue!)
                            
                            printInfo(data:"RETURNING REAL-TIME AVERAGE OF STATIONARY CMX POINTS")
                            
                            newMapLocationData = getAverageOfCMXLocations(locations: aggregatedStationaryList)
                        }
                        else if(currentMotion.motionState == .Moving){
                            printObservation(data:"MOVING LOGIC")

                            aggregatedMovingList.append(cmxLocationsValues.last!)
                            newMapLocationData = processMoving(locations: aggregatedMovingList)
                            
                            printInfo(data:"***************RETURNING REAL-TIME by Fusing Pace and CMX points **********")
                            
                           // newMapLocationData = cmxLocationsValues.last!
                            //newMapLocationData = getMovingAverageOfCMXLocations(locations: aggregatedMovingList)
                        }
                    }
                    
                    self.lastMotion = self.currentMotion
                    
                    if CMXFusionLocationConfigurations.shared.useSnapToPath{
                        newMapLocationData = getLocationByApplyingSnapToEdge(toLocation: newMapLocationData!)
                    }
                    //store as a local copy.
                    if self.lastMotion.motionState == CMXMotionState.Stationary {
                        newMapLocationData?.deviceActivity = "Stationary"
                    }else {
                        newMapLocationData?.deviceActivity = "Moving"
                    }
                    self.locationDataToApp = newMapLocationData
                    self.updateLocation?(newMapLocationData!)
                    
                }
                //            if newMapLocationData != nil {
                //                if previousLocationDataToSnapToPath != nil && previousCMXLocation != nil && currentCMXLocation != nil {
                //                    currentLocationDataToSnapToPath = applyDistanceFilter(previousCMXlocation: previousCMXLocation!, currentCMXLocation: currentCMXLocation!, thresholdDistance: 0)
                //                    previousLocationDataToSnapToPath = newMapLocationData!
                //                }
                //                else{
                //                    currentLocationDataToSnapToPath = newMapLocationData
                //                    previousLocationDataToSnapToPath = newMapLocationData
                //                }
                //            }
                //            locationDataToApp = getLocationByApplyingSnapToEdge(toLocation: currentLocationDataToSnapToPath!)
                //
                // self.updateLocation?(locationDataToApp!)
            }//using motion
        }else{
            updateFailure()
        }
        scheduleNextUpdate()
    }
}

// MARK: - Motion Detection
extension CMXFusionController {
    
    func startMotionDetectionUpdates() {
        motionDetectionController.startProcessing()
    }
    
    func getCurrentMotionState() -> CMXMotionState {
        return CMXMotionState.Moving
    }
    
}


// MARK: - CMX Location
extension CMXFusionController {
    
    func startCMXLocationUpdates(){
        // fetch location update from server
        CMXLc().startCMXLocationUpdates(self.macAddress, {[weak self] (cmxLocation) in guard let `self` = self else { return }
            if self.cmxLocationsValues.count > AppCfg().internalConfigurations.numberOfObservationsRequiredForFusionAlgorithm {
                self.cmxLocationsValues.removeFirst()
            }
            self.cmxLocationsValues.append(cmxLocation)
            self.ableToGetLocationUpdate = true
        }) { [weak self] (error) in guard let `self` = self else { return }
            self.ableToGetLocationUpdate = false
            printInfoErrorIfRequired(error: error)
        }
    }
    
}


// MARK: - Beacons
extension CMXFusionController {
    
    func startNearByBeaconUpdates(){
        Sc().startBeaconsUpdates({[weak self] (beaconsCollection) in
            guard let `self` = self else { return }
            for beacon in beaconsCollection.beacons! {
                //   printInfo(data: "beacon found \nmajorId\(beacon.beacon?.major)\nminorId\(beacon.beacon?.minor)\n\n")
            }
            self.nearByBeacons.append(beaconsCollection)
        }) { [weak self] (error) in guard let `self` = self else { return }
            printInfoErrorIfRequired(error: error)
        }
    }
}


// MARK: - Headings
extension CMXFusionController {
    
    func startHeadingUpdates(){
        Hc().startUpdatingHeadings(completion: {[weak self] (heading) in guard let `self` = self else { return }
            self.headings.append(heading)
        }) { [weak self] (error) in guard let `self` = self else { return }
            printInfoErrorIfRequired(error: error)
        }
    }
    
}


// MARK: - Path Matching
extension CMXFusionController {
    
    
    
}

// MARK: - Fusion Helpers
extension CMXFusionController {
    
    func applyDistanceFilter(previousCMXlocation:CMXLocationData,currentCMXLocation:CMXLocationData,thresholdDistance:Double)->CMXLocationData{
        let distance = sqrt(pow(previousCMXlocation.mapCoordinate.xValue() - currentCMXLocation.mapCoordinate.xValue(), 2) + pow(previousCMXlocation.mapCoordinate.yValue() - currentCMXLocation.mapCoordinate.yValue(), 2))
        if distance < thresholdDistance {
            return previousCMXlocation;
        }else{
            return currentCMXLocation
        }
    }
    
    func getAverageOfCMXLocations(locations:[CMXLocationData])->CMXLocationData{
        //WARNING: make this method genric and handle case when location count are 0
        let location = cmxLocationsValues.last!
        let timeThreshold = 8000.0 // latest 8 seconds
        var averageX = 0.0 as Double
        var averageY = 0.0 as Double
        var sumOfAllX = 0.0
        var sumOfAllY = 0.0
        var count = 0//cmxhfSafeDouble(cmxLocationsValues.count)
        let currentTime = Date()
        if locations != nil && locations.count > 0 {
            for locationCurr in locations {
                if ((currentTime.timeIntervalInMilliSecondsSince1970()) - locationCurr.mapCoordinate.changeTime) <= timeThreshold
                {
                    printInfo(data:"**********CMX VALUE WITHIN PAST 8 seconds*********")
                    
                    sumOfAllX = sumOfAllX + locationCurr.mapCoordinate.xValue()
                    sumOfAllY = sumOfAllY + locationCurr.mapCoordinate.yValue()
                    count += 1
                }
            }
            if count > 0{
                printInfo(data:"**********AVERAGE OF CMX VALUES WITHIN PAST 8 seconds*********")
                printInfo(data:"COUNT IN AVERAGE = \(count) OUT OF CMX LOCATIONS COUNT = \(cmxLocationsValues.count)")
                averageX = sumOfAllX / Double(count)
                averageY = sumOfAllY / Double(count)
                location.mapCoordinate.setXValue(value:averageX)
                location.mapCoordinate.setYValue(value: averageY)
            }
            else{
                printInfo(data:"**********NO CMX VALUE WITHIN PAST 8 seconds*********")
                printInfo(data:"**********RETURNING LAST CMX VALUE*********")
            }
        }
        
        location.mapCoordinate.changeTime = (Date().timeIntervalInMilliSecondsSince1970())
        return location
    }
    
    
    func getMovingAverageOfCMXLocations(locations:[CMXLocationData])->CMXLocationData{
        
        /*
         *
         **************************************      SUGGESTED ALOGORITHM       ****
         *
         *  Objective : TIME BASED MOVING AVERAGE
         *
         ***************************************************************************
         *
         *
         *  timeThrehold = 5; // in seconds; configurable
         *  //List is sorted on timestamp
         *
         *
         *  Point2D findTimeWeightedAverageCMX_XY(List,timeThreshold)
         *  currTime = currSystemTime();
         *  double weights  = new double [List.size()]
         *  double weightedSum = 0.0;
         *  double totalWeight = 0.0;
         *  for i from 0 to List.size(){
         *  if (currTime - t[i]<=timeThreshold){
         *  weights[i] =timeThreshold-(currTime - t[i])
         *  totalWeight+=weights[i]
         *  weightedSum+=(Pt[i].x*weights[i])//similar for t[i].y
         *  }
         *  }
         *
         *
         *  if (totalWeight>0){//div by zero check
         *  return weightedSum/totalWeight;
         *  }
         *  return 0;
         *
         */
        
        let timeThreshold : Double = 5000.0
        let currentTime = Date()
        var weights = [Double]()
        var weightedSumX = 0.0
        var weightedSumY = 0.0
        var totalWeight = 0.0
        var i = 0
        for l in locations {
            if ((currentTime.timeIntervalInMilliSecondsSince1970()) - l.mapCoordinate.changeTime) <= timeThreshold {
                weights.append(timeThreshold - ((currentTime.timeIntervalInMilliSecondsSince1970()) - (locations[i]).mapCoordinate.changeTime))
                totalWeight = totalWeight + weights[i]
                weightedSumX = weightedSumX + l.mapCoordinate.xValue() * weights[i]
                weightedSumY = weightedSumY + l.mapCoordinate.yValue() * weights[i]
                i += 1
            }
        }
        
        let location = cmxLocationsValues.last!
        
        if totalWeight > 0 {
            location.mapCoordinate.setXValue(value: weightedSumX/totalWeight)
            location.mapCoordinate.setYValue(value: weightedSumY/totalWeight)
            location.mapCoordinate.changeTime = Date().timeIntervalInMilliSecondsSince1970()
        }
        
        return location
    }
    
    func getLocationByApplyingSnapToEdge(toLocation:CMXLocationData)->CMXLocationData{
        
        func closestPointOnLineSegment(start p1: CGPoint, end p2: CGPoint , fromPoint:CGPoint) -> CGPoint {
            let v = CGPoint(x: p2.x - p1.x, y: p2.y - p1.y)
            var t : CGFloat = (fromPoint.x * v.x - p1.x * v.x + fromPoint.y * v.y - p1.y * v.y) / (v.x * v.x + v.y * v.y)
            if t < 0 {
                t = 0
            }
            else if t > 1 {
                t = 1
            }
            return CGPoint(x: p1.x + t * v.x,y:  p1.y + t * v.y)
        }
        
        func distance(start p1: CGPoint, end p2: CGPoint) -> Double {
            let p1x : Float = Float(p1.x)
            let p1y : Float  = Float(p1.y)
            let p2x : Float  = Float(p2.x)
            let p2y : Float  = Float(p2.y)
            return Double(hypotf(p1x - p2x, p1y - p2y))
        }
        
        func getNode(nodeId:Int)->CMXNodes?{
            for node in floorPath!.nodes! {
                if node.nodeId! == nodeId {
                    return node
                }
            }
            return nil
        }
        
        //Step 1:- Safety Check
        //if floor path is not fetched , skip this step
        if floorPath == nil || floorPath?.nodes == nil || floorPath?.nodes?.count == 0 || floorPath?.segments?.count == 0{
            printInfo(data: "Floor path was not loaded properly , skipping snap to nearest EDGE")
            return toLocation
        }
        
        //Step 2:-
        // load all nodes into array
        let nodes = floorPath?.nodes
        
        
        //Step 3:-
        //find the nearest node from current location (both start node and end node)
        var minimumDistance = 10240 as Double //some bigger number
        var nearestNode:CMXNodes?
        for node in nodes! {
            let startPoint = CGPoint(x: CGFloat(node.location!.x!), y:  CGFloat(node.location!.y!))
            let endPoint = CGPoint(x: CGFloat(toLocation.mapCoordinate.xValue()), y:  CGFloat(toLocation.mapCoordinate.yValue()))
            let distanceValue = distance(start: startPoint, end: endPoint)
            if distanceValue < minimumDistance{
                minimumDistance = distanceValue
                nearestNode = node
            }
        }
        
        //Step 4:-
        //find the nearest connecting node , with respect to nearesh node found
        let segments = floorPath?.segments
        var connectedSegments = [CMXSegments]()
        for segment in segments! {
            for nodeId in segment.startEndNodes! {
                if nodeId.intValue == nearestNode!.nodeId! {
                    connectedSegments.append(segment)
                }
            }
        }
        
        printInfo(data: "Total \(connectedSegments.count) Segments Found connected with nearest node")
        
        //Step 5:-
        //find the nearest segment from list of connected segments
        var minimumDistanceConnectingNode = 10240 as Double //some bigger number
        var nearestConnectingNode:CMXNodes?
        var nearestConnectingSegment:CMXSegments?
        for segment in connectedSegments {
            for nodeId in segment.startEndNodes! {
                if nodeId.intValue != nearestNode!.nodeId! {
                    let node = getNode(nodeId: nodeId.intValue)
                    if node != nil {
                        let startPoint = CGPoint(x: CGFloat(node!.location!.x!), y:  CGFloat(node!.location!.y!))
                        let endPoint = CGPoint(x: CGFloat(toLocation.mapCoordinate.xValue()), y:  CGFloat(toLocation.mapCoordinate.yValue()))
                        let distanceValue = distance(start: startPoint, end: endPoint)
                        if distanceValue < minimumDistanceConnectingNode{
                            minimumDistanceConnectingNode = distanceValue
                            nearestConnectingNode = node
                            nearestConnectingSegment = segment
                        }
                    }
                }
            }
        }
        
        if nearestConnectingSegment != nil {
            printInfo(data: "Nearest Connecting Segments Found")
        }else{
            printInfo(data: "Nearest Connecting Segments Not Found , Skipping snap to edge")
            return toLocation
        }
        
        //Step 6:-
        //calculate projection of point on line segment
        let projectedLocation : CGPoint?
        if nearestConnectingSegment?.startEndNodes!.count == 2 {
            let nodeStart = getNode(nodeId: nearestConnectingSegment!.startEndNodes![0].intValue)
            let nodeEnd = getNode(nodeId: nearestConnectingSegment!.startEndNodes![1].intValue)
            let nodePointStart = CGPoint(x: CGFloat(nodeStart!.location!.x!), y: CGFloat(nodeStart!.location!.y!))
            let nodePointEnd = CGPoint(x: CGFloat(nodeEnd!.location!.x!), y: CGFloat(nodeEnd!.location!.y!))
            let locationPoint = CGPoint(x: CGFloat(toLocation.mapCoordinate.xValue()), y: CGFloat(toLocation.mapCoordinate.yValue()))
            projectedLocation = closestPointOnLineSegment(start: nodePointStart, end: nodePointEnd, fromPoint: locationPoint)
        }else{
            printInfo(data: "Segments not valid , Skipping snap to edge")
            return toLocation
        }
        
        
        //Step 7:-
        //return the location
        if projectedLocation != nil{
            printInfo(data: "CMX XY (\(toLocation.mapCoordinate.xValue()),\(toLocation.mapCoordinate.yValue())) , Snap to Edge (\(projectedLocation!.x),\(projectedLocation!.y))")
        }else{
            printInfo(data: "Unable to project point on line segment, Skipping snap to edge")
            return toLocation
        }
        
        toLocation.mapCoordinate.setXValue(value: Double(projectedLocation!.x))
        toLocation.mapCoordinate.setYValue(value: Double(projectedLocation!.y))
        
        return toLocation
    }
    
    func processTransition(previousMotion:CMXMotion,currentMotion:CMXMotion) -> CMXLocationData {
        
        let cadenceThresholdForStationaryToMovingTransition = 8/11 as Double
        let timeThresholdForStationaryToMovingTransition = 11000 as Double
        let timeThresholdForMovingToStationaryTransition = 3000 as Double
        
        if previousMotion.motionState == .Stationary && currentMotion.motionState == .Moving {
            printInfo(data:"====================TRANSITION: STATIONARY TO MOVING LOGIC========================")
            
            self.aggregatedMovingList.removeAll()
            self.aggregatedMovingList = [CMXLocationData]()
            
            if cmxhfSafeDouble(currentMotion.getCadence) > cadenceThresholdForStationaryToMovingTransition {
                
                printInfo(data:"***************CADENCE WAS HIGH IN THE PAST x SECONDS OF STARTING TO WALK**********")

                for location in cmxLocationsValues {
                    if  ((currentMotion.timeStamp.timeIntervalInMilliSecondsSince1970()) - location.mapCoordinate.changeTime) <= timeThresholdForStationaryToMovingTransition {
                        printInfo(data:"***************ADDING LAST x SECONDS OF DATA TO MOVING AGGREGATION LIST**********")
                        
                        
                        self.aggregatedMovingList.append(location)
                    }
                }
              
            }
            printInfo(data: "AGGREGATEDMOVINGLIST: \(self.aggregatedMovingList)" )
            //return
            //            if self.aggregatedMovingList != nil && self.aggregatedMovingList.count > 0 {
            //                printInfo(data:"***************RETURNING FROM MOVING AGGREGATION LIST**********")
            //                return self.aggregatedMovingList.last!
            //            }
            
                        //return self.cmxLocationsValues.last!
            //getMovingAverageOfCMXLocations(locations: aggregatedMovingList)
            if self.aggregatedMovingList.count > 0 {
                printInfo(data:"***************PROCESS MOVING OVER AGGREGATION LIST**********")
                return processMoving(locations: aggregatedMovingList)
            }
            else{
                printInfo(data:"***************NOTHING IN MOVING AGGREGATION LIST, outputting the old bluetooth value**********")
                return self.locationDataToApp!
            }
            
        }else if previousMotion.motionState == .Moving && currentMotion.motionState == .Stationary {
            printInfo(data:"====================TRANSITION: MOVING TO STATIONARY LOGIC========================")
            
            self.aggregatedStationaryList.removeAll()
            self.aggregatedStationaryList = [CMXLocationData]()
            for location in cmxLocationsValues {
                //                if location.capturedAt.timeIntervalSince1970 > ( currentMotion.timeStamp.timeIntervalSince1970 - timeThresholdForMovingToStationaryTransition ) {
                if ((currentMotion.timeStamp.timeIntervalInMilliSecondsSince1970()) - location.mapCoordinate.changeTime) <= timeThresholdForMovingToStationaryTransition {
                    printInfo(data:"***************ADDING LAST 3 SECONDS OF DATA TO STATIONARY LIST**********")
                    self.aggregatedStationaryList.append(location)
                }
            }
            if self.aggregatedStationaryList != nil && self.aggregatedStationaryList.count > 0 {
                printInfo(data:"***************RETURNING REAL-TIME AVERAGE OF STATIONARY AGGREGATION LIST**********")
                return getAverageOfCMXLocations(locations: aggregatedStationaryList)
            }
            return cmxLocationsValues.last!
        }
        //This should never happen
        return getAverageOfCMXLocations(locations: aggregatedStationaryList)
    }
    
    
    /* moving logic
     */
    
//    func curveFittingLinear(locations: [CMXLocationData!]){
//        let currentTimeInMillis = Date().timeIntervalInMilliSecondsSince1970()
//        let timeThreshold = 10000 // 10 seconds
//        var locationsWithinTime = [CMXLocationData]()
//        
//        for loc in locations {
//            if (currentTimeInMillis - loc.mapcoordinate.changeTime) <= timeThreshold{
//                locationsWithinTime.append(loc)
//            }
//        }
//        
//        var sumX = 0.0
//        var sumY = 0.0
//        var sumXY = 0.0
//        var sumX2 = 0.0
//        
//        for l in locationsWithinTime{
//            sumX = sumX + l.mapCoordinate.xValue()
//            sumX2 = sumX2 + (l.mapCoordinate.xValue()*l.mapCoordinate.xValue())
//            sumY = sumY + l.mapCoordinate.yValue()
//            sumXY = sumXY + (l.mapCoordinate.xValue())*l.mapCoordinate.yValue()
//        }
//        
//        let c = ((sumX2*sumY - sumX*sumXY)*1.0/(locationsWithinTime.count*sumX2-sumX*sumX)*1.0)
//        let m = ((locationsWithinTime.count*sumXY-sumX*sumY)*1.0/(locationsWithinTime.count*sumX2-sumX*sumX)*1.0)
//        
//    }
    
    func processMoving(locations:[CMXLocationData])->CMXLocationData{
        
        /*
         *Function 1:
         Point2D averagedForMoving (aggregatedMovingList, currentMotion.Pace, currentStatePt){
         //if we have not received new CMX data since last update to bluedot
         if currCMX_XY.timestamp < currentStatePt.timestamp
         {
         //Overall: take slope of the CMX XY, in the same direction propagate currPt by the pace
         Return applyPaceToState(currCMXXY,prevCMXXY, currentStatePt, currentMotion.pace);
         }
         else{ // received a new CMX_XY; weighted value
         newLocation = weightedAvg(newCMX_XY, w1,applyPaceToState(currCMXXY,prevCMXXY, currentStatePt, currentMotion.pace), w2)
         //w1 and w2 are parameterized
         }
         }
         */
        let toLocation = locations.last!
        
        
        if locations.count > 1 && self.locationDataToApp != nil {
            
            
            let prevLocation = locations[locations.count-2]
            
            if (toLocation.mapCoordinate.changeTime < (self.locationDataToApp!.mapCoordinate.changeTime)) {
                printInfo(data: "no new CMX data has come.")
                
                return applyPaceToState(currentMotion: currentMotion, previousCMXLocation: prevLocation, currentCMXLocation: toLocation, locationDataToApp: locationDataToApp!)
                
            }
            else{
                printInfo(data: "New CMX data has come, so fusing pace and CMX data.")
                
                let paceAppliedPoint = applyPaceToState(currentMotion: currentMotion, previousCMXLocation: prevLocation, currentCMXLocation: toLocation, locationDataToApp: locationDataToApp!)
                return weightedAvg(currentCMXLocation: toLocation, wCurrCMX: 0.5, ptWithPaceApplied: paceAppliedPoint, wPace: 0.5)
            }
            
        }
        else{
            printInfo(data: "have only 1 cmx xy, returning latest.")
        }
        toLocation.mapCoordinate.changeTime = (Date().timeIntervalInMilliSecondsSince1970())
        
        
        return toLocation
    }
   
//    func processMoving(locations:[CMXLocationData])->CMXLocationData{
//        
//        /*
//         *Function 1:
//         Point2D averagedForMoving (aggregatedMovingList, currentMotion.Pace, currentStatePt){
//         //if we have not received new CMX data since last update to bluedot
//         if currCMX_XY.timestamp < currentStatePt.timestamp
//         {
//         //Overall: take slope of the CMX XY, in the same direction propagate currPt by the pace
//         Return applyPaceToState(currCMXXY,prevCMXXY, currentStatePt, currentMotion.pace);
//         }
//         else{ // received a new CMX_XY; weighted value
//         newLocation = weightedAvg(newCMX_XY, w1,applyPaceToState(currCMXXY,prevCMXXY, currentStatePt, currentMotion.pace), w2)
//         //w1 and w2 are parameterized
//         }
//         }
//         */
//        let toLocation = cmxLocationsValues.last!
//        
//        if cmxLocationsValues.count > 1 && self.locationDataToApp != nil {
//            
//            
//            let prevLocation = cmxLocationsValues[cmxLocationsValues.count-2]
//            
//            if (toLocation.mapCoordinate.changeTime < (self.locationDataToApp!.mapCoordinate.changeTime)) {
//                printInfo(data: "no new CMX data has come.")
//                
//                return applyPaceToState(currentMotion: currentMotion, previousCMXLocation: prevLocation, currentCMXLocation: toLocation, locationDataToApp: locationDataToApp!)
//                
//            }
//            else{
//                printInfo(data: "New CMX data has come, so fusing pace and CMX data.")
//                
//                let paceAppliedPoint = applyPaceToState(currentMotion: currentMotion, previousCMXLocation: prevLocation, currentCMXLocation: toLocation, locationDataToApp: locationDataToApp!)
//                return weightedAvg(currentCMXLocation: toLocation, wCurrCMX: 0.5, ptWithPaceApplied: paceAppliedPoint, wPace: 0.5)
//            }
//            
//        }
//        else{
//            printInfo(data: "have only 1 cmx xy, returning latest.")
//        }
//        toLocation.mapCoordinate.changeTime = (Date().timeIntervalInMilliSecondsSince1970())
//        
//        
//        return toLocation
//    }
    
    
    
    /*
     Function 2:
     applyPaceToState(currCMXXY, prevCMXXY, currentStatePt, pace){
     
     //Overall: take slope of the CMX XY, in the same direction propagate currPt by the pace
     Theta = tan_Inverse((CurrCMX_Y-PrevCMX_Y)/(CurrCMX_Y-PrevCMX_Y))
     //handle div by zero
     
     //Also speed = (1/ pace) — as pace is in meters
     //speed_in_feet = speed*3.28084
     //Use this speed_in_feet
     
     //So logic is
     
     if pace >0
     speed_in_feet = (1/pace)*3.28084
     
     computedX = currStatPt.x + speed_in_feet*cos(theta)
     computedY= currStatPt.y+ speed_in_feet*sin(theta)
     
     Return new Point2D(computedX,computedY)
     }
     */
    
    func applyPaceToState(currentMotion:CMXMotion, previousCMXLocation:CMXLocationData, currentCMXLocation:CMXLocationData, locationDataToApp:CMXLocationData) -> CMXLocationData{
        
        //        let currCMXValue = cmxLocationsValues.last
        //
        //        for location in cmxLocationsValues {
        //            if (location.capturedAt.timeIntervalSince1970 < currCMXValue?.capturedAt.timeIntervalSince1970){
        //                let prevCMXValue = location
        //            }
        //        }
        
        //computing heading
        let toLocation = cmxLocationsValues.last!
        
        let theta = atan2((currentCMXLocation.mapCoordinate.xValue() - previousCMXLocation.mapCoordinate.xValue()), (currentCMXLocation.mapCoordinate.yValue() - previousCMXLocation.mapCoordinate.yValue()))
        
        let currentPace = cmxhfSafeDouble(currentMotion.pace) // seconds/meter
        if currentMotion.motionState == .Moving && currentPace > 0{
            let speedInFeet = (1/currentPace)*3.28084 // feet/seconds
            toLocation.mapCoordinate.setXValue(value: Double(locationDataToApp.mapCoordinate.xValue()+speedInFeet * cos(theta)))
            toLocation.mapCoordinate.setYValue(value: Double(locationDataToApp.mapCoordinate.yValue()+speedInFeet * sin(theta)))
        }
        toLocation.mapCoordinate.changeTime = (Date().timeIntervalInMilliSecondsSince1970())
        return toLocation
    }
    
    
    //weighted between CMX and pace
    
    func weightedAvg(currentCMXLocation:CMXLocationData, wCurrCMX: Double, ptWithPaceApplied: CMXLocationData, wPace: Double) -> CMXLocationData{
        //start with weights w1 = 1, w2 = 1 ==> Equal weights; configurable
        
        let toLocation = cmxLocationsValues.last!
        if wCurrCMX+wPace > 0{
            toLocation.mapCoordinate.setXValue(value: Double((wCurrCMX*currentCMXLocation.mapCoordinate.xValue()+wPace * ptWithPaceApplied.mapCoordinate.xValue())/(wCurrCMX+wPace)))
            
            toLocation.mapCoordinate.setYValue(value: Double((wCurrCMX*currentCMXLocation.mapCoordinate.yValue()+wPace * ptWithPaceApplied.mapCoordinate.yValue())/(wCurrCMX+wPace)))
            
        }
        
        
        //    computedX = ((w1* newCMX_X) + (w2*ptWithPaceApplied.x))/(w1+w2);
        //    computedY = ((w1* newCMX_Y) + (w2*ptWithPaceApplied.y))/(w1+w2);
        //
        
        
        toLocation.mapCoordinate.changeTime = (Date().timeIntervalInMilliSecondsSince1970())
        
        return toLocation
    }
    /* ====END OF MOVING LOGIC*/
    
    
    
    
    //    func averagedForMoving(aggregatedMovingList:[CMXLocationData],pace:CMXPace,currentLocationData:CMXLocationData,currentMotion:CMXMotion) -> CMXLocationData {
    //
    //        /**
    //
    //         Function 1:
    //         Point2D averagedForMoving (aggregatedMovingList, currentMotion.Pace, currentStatePts{
    //         //if we have not received new CMX data since last update to bluedot
    //         if currCMX_XY.timestamp < currentStatePt.timestamp
    //         {
    //         //Overall: take slope of the CMX XY, in the same direction propagate currPt by the pace
    //         Return applyPaceToState(currCMXXY,prevCMXXY, currentStatePt, currentMotion.pace);
    //         }
    //         else{ // received a new CMX_XY; weighted value
    //         newLocation = weightedAvg(newCMX_XY, w1,applyPaceToState(currCMXXY,prevCMXXY, currentStatePt, currentMotion.pace), w2)
    //         //w1 and w2 are parameterized
    //         }
    //         }
    //
    //         */
    //
    //        func applyPace(currentLocationData:CMXLocationData,previousLocationData:CMXLocationData,currentMotion:CMXMotion,pace:CMXPace) -> CMXLocationData {
    //            //Overall: take slope of the CMX XY, in the same direction propagate currPt by the pace
    //            let theta = atan((currentLocationData.mapCoordinate.yValue()-previousLocationData.mapCoordinate.yValue())/(currentLocationData.mapCoordinate.xValue()-previousLocationData.mapCoordinate.xValue()))
    //            let speed = (1/pace.pace.doubleValue)*3.28084
    //            if pace.pace.doubleValue > 0 {
    //                let newXValue = currentLocationData.mapCoordinate.xValue() + speed*cos(theta)
    //                let newYValue = currentLocationData.mapCoordinate.yValue() + speed*sin(theta)
    //                currentLocationData.mapCoordinate.setXValue(value: newXValue)
    //                currentLocationData.mapCoordinate.setYValue(value: newYValue)
    //            }
    //        }
    //
    //        func weightedAverage(currentLocationData:CMXLocationData,previousLocationData:CMXLocationData,currentMotion:CMXMotion,pace:CMXPace) -> CMXLocationData {
    //            //Overall: take slope of the CMX XY, in the same direction propagate currPt by the pace
    //            let theta = atan((currentLocationData.mapCoordinate.yValue()-previousLocationData.mapCoordinate.yValue())/(currentLocationData.mapCoordinate.xValue()-previousLocationData.mapCoordinate.xValue()))
    //            let speed = (1/pace.pace.doubleValue)*3.28084
    //            if pace.pace.doubleValue > 0 {
    //                let newXValue = currentLocationData.mapCoordinate.xValue() + speed*cos(theta)
    //                let newYValue = currentLocationData.mapCoordinate.yValue() + speed*sin(theta)
    //                currentLocationData.mapCoordinate.setXValue(value: newXValue)
    //                currentLocationData.mapCoordinate.setYValue(value: newYValue)
    //            }
    //        }
    //
    //
    //        if currentLocationData.capturedAt < currentMotion.timeStamp {
    //            return applyPace(currentLocationData: currentLocationData, previousLocationData: currentLocationData, currentMotion: currentMotion, pace: pace)
    //        }
    //        else{ // received a new CMX_XY; weighted value
    //            newLocation = weightedAvg(newCMX_XY, w1,applyPaceToState(currCMXXY,prevCMXXY, currentStatePt, currentMotion.pace), w2)
    //            //w1 and w2 are parameterized
    //        }
    //
    //    }
    
    
    /*
     * location from nearby beacons logic
     */
    func getLocationFromNearByBeacons_v1(validMarkers:[CMXBleMarkers])->CMXLocationData{
        //step 1
        
        
        //        let currTime = Date().timeIntervalInMilliSecondsSince1970()
        //        let timeThresholdInMillis = 4000.00
        let rssiThreshold = -60
        let accuracyThreshold = 1.5
        let toLocation = cmxLocationsValues.last!
        if self.nearByBeacons != nil && self.floorPath?.bleMarkers != nil {
            
            self.nearByBeaconsFormatted.append(getFormattedBeaconsDataObject(beaconsCollection: self.nearByBeacons.last!, validMarkers: self.floorPath!.bleMarkers!))
            
            let currentBeacons = nearByBeaconsFormatted.last!
            for marker in validMarkers {
                for b in currentBeacons.beaconsInfo!{
                    
                    //                    printInfo(data:"minor id \(b.getMinorID())")
                    //                    printInfo(data:"proximity \(b.getProximity())")
                    //
                    if marker.minorId == b.getMinorID() && (b.getProximity() == 2 || b.getProximity() == 1) {
                        toLocation.mapCoordinate.setXValue(value: Double(marker.xFt!))
                        toLocation.mapCoordinate.setYValue(value: Double(marker.yFt!))
                        printInfo(data:"**************NEAR BEACON********")
                        printInfo(data:marker.minorId)
                        
                        break
                    }
                }
                
            }
        }
        
        //        func getLocationFromNearByBeacons_v2(validMarkers:[CMXBleMarkers])->CMXLocationData{
        //            //step 1
        //
        //
        //            let currTime = Date().timeIntervalInMilliSecondsSince1970()
        //            let timeThresholdInMillis = 4000.00
        //            let rssiThreshold = -60
        //            let accuracyThreshold = 1.5
        //            let toLocation = cmxLocationsValues.last!
        //
        //
        //
        //            //collect all beacons within the past 4 seconds.
        //            if self.nearByBeacons != nil && self.floorPath?.bleMarkers != nil {
        //                // collect all beacons from the current environment.
        //                for beacons in self.nearByBeacons{
        //                    if (currTime - beacons.getTimestamp().timeIntervalInMilliSecondsSince1970()) <= timeThresholdInMillis{
        //                        self.nearByBeaconsFormatted.append(getFormattedBeaconsDataObject(beaconsCollection: beacons, validMarkers: self.floorPath!.bleMarkers!))
        //
        //                    }
        //                }
        //                minorid: List of rssi
        //https://github.com/raywenderlich/swift-algorithm-club/tree/master/Hash%20Table
        // Hashmap key : ArrayList<long>
        //minorID: List of prox
        //minorID : list of accuracy
        // 90:  [-65,-45,-75,-140]
        //minorId : [0,2,2,3] // proximity
        //majority voting =2
        //1 or 2
        //
        //                //get the median of
        //                for currentBeacons in self.nearByBeaconsFormatted
        //                for marker in validMarkers {
        //                    for b in currentBeacons.beaconsInfo!{
        //
        //                        //                    printInfo(data:"minor id \(b.getMinorID())")
        //                        //                    printInfo(data:"proximity \(b.getProximity())")
        //                        //
        //                        if marker.minorId == b.getMinorID() && (b.getProximity() == 2 || b.getProximity() == 1) {
        //                            toLocation.mapCoordinate.setXValue(value: Double(marker.xFt!))
        //                            toLocation.mapCoordinate.setYValue(value: Double(marker.yFt!))
        //                            printInfo(data:"**************NEAR BEACON********")
        //                            printInfo(data:marker.minorId)
        //
        //                            break
        //                        }
        //                    }
        //                    var bleMedianDictionary = [NSNumber : Double]()
        //                }
        //            }
        //                }
        //                }
        //
        
        
        
        toLocation.mapCoordinate.changeTime = (Date().timeIntervalInMilliSecondsSince1970())
        return toLocation
    }
    
    //    func getLocationFromNearByBeacons_v3(validMarkers:[CMXBleMarkers])->CMXLocationData{
    //        let  rssiThreshold = -65
    //        let accuracyThreshold = 1.5
    //        //     var nearByBeaconsWithinTime = [CMXBeaconCollectionFormatted]()
    //        //     for b in nearByBeaconsFormatted {
    //        //     if currTime - b.getTimestamp().timeIntervalSince1970 < timeThresholdInMillis {
    //        //     nearByBeaconsWithinTime.append(b)
    //        //     }
    //        //     }
    //
    //        var dic = [NSNumber : Double]()
    //
    //        let toLocation = cmxLocationsValues.last!
    //        //var toLocation:CMXLocationData? = nil
    //        for nB in nearByBeaconsFormatted{
    //        let currentBeacons = nB
    //            if (currTime - (currentBeacons.getTimestamp().timeIntervalInMilliSecondsSince1970())) < timeThresholdInMillis {
    //                for marker in validMarkers {
    //                    for b in currentBeacons.beaconsInfo!{
    //                        if marker.minorId == b.getMinorID() && (b.getRSSI() >= rssiThreshold || b.getAccuracy() <= accuracyThreshold || b.getProximity() == 2 || b.getProximity() == 1){
    //                                                                       break
    //                        }
    //                    }
    //
    //                }
    //        }
    //        }
    //        printInfo(data:"**************NEAR BEACON********")
    //        printInfo(data:marker.minorId)
    //
    //
    //        toLocation.mapCoordinate.setXValue(value: Double(marker.xFt!))
    //        toLocation.mapCoordinate.setYValue(value: Double(marker.yFt!))
    //        toLocation.mapCoordinate.changeTime = (Date().timeIntervalInMilliSecondsSince1970())
    //
    //        return toLocation
    //
    //
    //
    //    }
    
    
    func getFormattedBeaconsDataObject(beaconsCollection:CMXBeaconCollection,validMarkers:[CMXBleMarkers]) -> CMXBeaconCollectionFormatted {
        
        printInfo(data:"*******FORMATTED BEACONS FUNCTION***********")
        
        var cmxBeaconsInfo = [CMXBeaconInfo]()
        
        //Step 3:
        for marker in validMarkers {
            
            cmxBeaconsInfo.append(CMXBeaconInfo(minor: marker.minorId!, major: marker.majorId!, prox: getProximity(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker,beaconsCollection:beaconsCollection)), accu: getAccuracy(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker,beaconsCollection:beaconsCollection)), rssi: getRSSI(beacon: getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker: marker,beaconsCollection:beaconsCollection))))
            //printInfo(data:"number of beacons heard now: \(cmxBeaconsInfo.count)")
        }
        
        //Step 4:
        return CMXBeaconCollectionFormatted(beaconsInfo: cmxBeaconsInfo,timeStamp: beaconsCollection.timeStamp)
    }
    
    func getBeaconSpecifiedInBLEMarkerFromScannedBeacons(marker:CMXBleMarkers,beaconsCollection:CMXBeaconCollection) -> CLBeacon? {
        if marker.majorId != nil && marker.minorId != nil {
            for beacon in beaconsCollection.beacons! {
                if beacon.minor.intValue == marker.minorId! {
                    return beacon
                }
            }
        }
        return nil
    }
    
    func getProximity(beacon: CLBeacon?) -> Int {
        if (beacon == nil) { return 0 }
        return Int(beacon!.proximity.rawValue)
    }
    
    func getAccuracy(beacon: CLBeacon?) -> Double {
        if (beacon == nil) { return -1 }
        return Double(beacon!.accuracy)
    }
    
    func getRSSI(beacon: CLBeacon?) -> Int {
        if (beacon == nil) { return -140 }
        return Int(beacon!.rssi)
    }
    
    
    /*========END OF BEACON LOGIC==============*/
    
    
}
