//
//  CMXFloorDimension.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

/// Data modal to hold floor dimension
public class CMXFloorDimension: CMXCapturableData {
    /// <#Description#>
    public var height = 0.0
    /// <#Description#>
    public var width = 0.0
    /// <#Description#>
    public var length = 0.0
    /// <#Description#>
    public var offsetX = 0.0
    /// <#Description#>
    public var offsetY = 0.0
    /// <#Description#>
    public var unit = "FEET"

    /// <#Description#>
    ///
    /// - parameter height:  <#height description#>
    /// - parameter width:   <#width description#>
    /// - parameter length:  <#length description#>
    /// - parameter offsetX: <#offsetX description#>
    /// - parameter offsetY: <#offsetY description#>
    /// - parameter unit:    <#unit description#>
    ///
    /// - returns: <#return value description#>
    init(height:Double,width:Double,length:Double,offsetX:Double,offsetY:Double,unit:String) {
        self.height = height
        self.width = width
        self.length = length
        self.offsetX = offsetX
        self.offsetY = offsetY
        self.unit = unit
    }
}
