//
//  CMXPedometerDataService.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation
import CoreMotion

class CMXPedometerDataService: NSObject {
    var currentPedometerData = CMXPedometerData()
    var previousPedometerData = CMXPedometerData()
    var pedometerStateArray = [CMXMotion]()
//    var pedometerCadenceArray = [NSNumber]()
//    var pedometerPaceArray = [NSNumber]()
    var pedometerDistanceStepsArray = [CMXDistanceSteps]()
  //  var pedometerStepsArray = []
    //add pace array
    
    func startProcessing(){
        Sc().startPedometerUpdates {[weak self] (pedometerData) in guard let `self` = self else {
            return }
            
//            if Pdsc().furtherRefineCadenceValues {
//                if Pdsc().refineCadenceUsingAveragingUsingLastTwoObservations {
//                    
//                    /*
//                     *
//                     **************************************      SUGGESTED ALOGORITHM       ****
//                     *
//                     *  Objective : TO RE CALCULATE THE CADENCE BASED UPON OUR LOGIC
//                     *
//                     ***************************************************************************
//                     *
//                     *  PrevPdrData.cadence <- (CurrPdrData.numberOfSteps – PrevPdrData.numberOfSteps) / (CurrPdrData.timestamp – PrevPdrData.timestamp)
//                     *  PrevPdrData.numberOfSteps <- CurrPdrData.numberOfSteps
//                     *  PrevPdrData. timestamp <- CurrPdrData.timestamp
//                     *
//                     ***************************************************************************
//                     *
//                     */
//                    
//                    let deltaNumberOfSteps = (cmxhfSafeDouble(pedometerData.numberOfSteps?.doubleValue) - cmxhfSafeDouble(self.previousPedometerData.currentCadence?.cadence))
//                    let deltaTimeInterval = (cmxhfSafeDouble(pedometerData.endDate?.timeIntervalSince1970) - cmxhfSafeDouble(self.previousPedometerData.endDate?.timeIntervalSince1970))
//                    self.previousPedometerData = pedometerData // replaced object , so replaced everything with latest
//                    let cadence = CMXCadence(cadence: NSNumber(value: deltaNumberOfSteps/deltaTimeInterval), timeStamp: Date())
//                    
//                    self.previousPedometerData = pedometerData
//                    self.previousPedometerData.currentCadence = cadence
//                    self.pedometerCadenceArray.append(cadence)
//                
//                }//else if Pdsc().refineCadenceUsingMovingAveragingLastFewSecondsObservations{
//
//                    /*
//                     *
//                     **************************************      SUGGESTED ALOGORITHM       ****
//                     *
//                     *  Objective : TO RE CALCULATE THE CADENCE USING MOVING AVERAGE
//                     *
//                     ***************************************************************************
//                     *
//                     *  timeThrehold = 5; // in seconds; configurable
//                     *  //List is sorted on timestamp
//                     *
//                     *
//                     *  double findTimeWeightedAverage(List,timeThreshold)
//                     *  currTime = currSystemTime();
//                     *  double weights  = new double [List.size()]
//                     *  double weightedSum = 0.0;
//                     *  double totalWeight = 0.0;
//                     *  for i from 0 to List.size(){
//                     *  if (currTime - t[i]<=timeThreshold){
//                     *  weights[i] =timeThreshold-(currTime - t[i])
//                     *  totalWeight+=weights[i]
//                     *  weightedSum+=(pace[i]*weights[i])
//                     *  }
//                     *  }
//                     *
//                     *
//                     *  if (totalWeight>0){
//                     *  return weightedSum/totalWeight;
//                     *  }
//                     *  return 0;
//                     *
//                     ***************************************************************************
//                     *
//                     */
//                    
//                    var weights = [Double]()
//                    var weightedSum = 0.0 as Double
//                    var totalWeight = 0.0 as Double
//                    
////                    for i in 0...self.pedometerCadenceArray.count {
////                        if self.pedometerCadenceArray[i].timeStamp.timeIntervalSinceNow <= Pdsc().movingAverageTimeThreshold{
////                            weights[i] = self.pedometerCadenceArray[i].timeStamp.timeIntervalSinceNow
////                            totalWeight = totalWeight + weights[i]
////                            weightedSum = weightedSum + cmxhfSafeDouble(self.pedometerCadenceArray[i].cadence)*weights[i]
////                        }
////                    }
//                  
//                    if totalWeight > 0 {
//                        
//                        let cadence = CMXCadence(cadence: NSNumber(value: weightedSum/totalWeight), timeStamp: Date())
//                      
//                        self.previousPedometerData = pedometerData
//                        self.previousPedometerData.currentCadence = cadence
//                        self.pedometerCadenceArray.append(cadence)
//                    
//                    }else{
//                    
//                        let cadence = CMXCadence(cadence: NSNumber(value: 0), timeStamp: Date())
//                        
//                        self.previousPedometerData = pedometerData
//                        self.previousPedometerData.currentCadence = cadence
//                        self.pedometerCadenceArray.append(cadence)
//                    
//                    }
//                }
 //           }else{
                
//                self.previousPedometerData = pedometerData
//               
//                if pedometerData.currentCadence != nil {
//                    self.pedometerCadenceArray.append(pedometerData.currentCadence!)
//                }
            
         //   }
            
            
            /*
             *
             **************************************      SUGGESTED ALOGORITHM       ****
             *
             *  Objective : TO RE CALCULATE THE PACE USING MOVING AVERAGE
             *
             ***************************************************************************
             *
             ***************************************************************************
             *
             */
            
//            var weights = [Double]()
//            var weightedSum = 0.0 as Double
//            var totalWeight = 0.0 as Double
////            for i in 0...self.pedometerPaceArray.count {
////                if self.pedometerPaceArray[i].timeStamp.timeIntervalSinceNow <= Pdsc().movingAverageTimeThreshold{
////                    weights[i] = self.pedometerPaceArray[i].timeStamp.timeIntervalSinceNow
////                    totalWeight = totalWeight + weights[i]
////                    weightedSum = weightedSum + cmxhfSafeDouble(self.pedometerPaceArray[i].pace)*weights[i]
////                }
////            }
//            if totalWeight > 0 {
//                let pace = CMXPace(pace: NSNumber(value: weightedSum/totalWeight), timeStamp: Date())
//                self.pedometerPaceArray.append(pace)
//            }else{
//                let pace = CMXPace(pace: NSNumber(value: 0), timeStamp: Date())
//                self.pedometerPaceArray.append(pace)
//            }
          //  if(pedometerData != nil){
            printInfo(data: "***********PEDOMETER START PROCESSING********")
            
             let currentTime = Date()
            if pedometerData.currentCadence != nil {
                
                    printInfo(data: "************PEDOMETER MOVING*****************************************")

                printInfo(data: "latest pedometer timestamp= \(pedometerData.endDate.timeIntervalInMilliSecondsSince1970())")
               
                printInfo(data: "AT CURRENT TIME= \(currentTime.timeIntervalInMilliSecondsSince1970())")
                
                if currentTime.timeIntervalInMilliSecondsSince1970() - pedometerData.endDate.timeIntervalInMilliSecondsSince1970() <= Pdsc().pedometerStaleTimeThreshold {
                self.pedometerStateArray.append(CMXMotion(motionState: .Moving, originalCadence: cmxhfSafeDouble(pedometerData.currentCadence), pace: cmxhfSafeDouble(pedometerData.currentPace), timeStamp: pedometerData.endDate))
                }
                else{
                    self.pedometerStateArray.append(CMXMotion(motionState: .Stationary, originalCadence: 0.0 , pace:  0.0 , timeStamp: Date()))
                }
            }
            else{
                
                printInfo(data:"************PEDOMETER STATIONARY*****************************************")
      
                printInfo(data: "AT CURRENT TIME= \(currentTime.timeIntervalInMilliSecondsSince1970())")
                self.pedometerStateArray.append(CMXMotion(motionState: .Stationary, originalCadence: 0.0 , pace:  0.0 , timeStamp: Date()))

                

                }
//            }
//                else if currentTime-(pedometerData.capturedAt.timeIntervalInMilliSecondsSince1970()) > timeThreshold{
//                
//                    printInfo(data:"************PEDOMETER STATIONARY*****************************************")
//
//                    self.pedometerStateArray.append(CMXMotion(motionState: .Stationary, timeStamp: Date()))
//                }
//                }
//                else{
//                    printInfo(data:"************NO PEDOMETER DATA*******")
//                }
            
            }
    }
}
