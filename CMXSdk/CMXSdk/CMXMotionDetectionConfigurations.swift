//
//  CMXFusionLocationConfigurations.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

import Foundation

class CMXMotionDetectionConfigurations: NSObject {
    /// initialize instance
    static let shared : CMXMotionDetectionConfigurations = {
        let instance = CMXMotionDetectionConfigurations()
        return instance
    }()
   
    override init() {
    }

    var useComputedCadence = true
    var motionStaleTimeThreshold = 5000.0 //in milliseconds
}
