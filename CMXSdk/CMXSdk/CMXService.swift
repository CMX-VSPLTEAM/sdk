//
//  CMXService.swift
//  CMXSdk
//
/**
 * Copyright 2016-17 by Cisco Systems
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Cisco Systems,  ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Cisco Systems.
 */

/**
 *
 * Virinchi Softwares Pvt Ltd
 *
 */

// Content Description : This is a DataModel to hold service details , services are a part of cmx server i.e CMXServer , cmx server can have multiple services active

import Foundation

/// <#Description#>
public class CMXService: NSObject {
    
    /// <#Description#>
    public  var dnsName : String?
    /// <#Description#>
    public  var lifeCycleStatus : String?
    /// <#Description#>
    public  var nodeName : String?
    /// <#Description#>
    public  var port : String?
    /// <#Description#>
    public  var properties : String?
    /// <#Description#>
    public  var serviceName : String?
    /// <#Description#>
    public  var type : String?
    /// <#Description#>
    public  var weight : String?
    
    /// <#Description#>
    ///
    /// - parameter dnsName:         <#dnsName description#>
    /// - parameter lifeCycleStatus: <#lifeCycleStatus description#>
    /// - parameter nodeName:        <#nodeName description#>
    /// - parameter port:            <#port description#>
    /// - parameter properties:      <#properties description#>
    /// - parameter serviceName:     <#serviceName description#>
    /// - parameter type:            <#type description#>
    /// - parameter weight:          <#weight description#>
    ///
    /// - returns: <#return value description#>
    init(dnsName:String,lifeCycleStatus:String,nodeName:String,port:String?,properties:String,serviceName:String?,type:String?,weight:String?) {
        self.dnsName = dnsName
        self.lifeCycleStatus = lifeCycleStatus
        self.nodeName = nodeName
        self.port = port
        self.properties = properties
        self.serviceName = serviceName
        self.type = type
        self.weight = weight
    }
}
