//
//  CMXAddServerViewController.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//


import UIKit
import Foundation
import SwiftValidators
//import CMXSdk

/// <#Description#>
class CMXAddServerViewController: UITableViewController {
    
    @IBOutlet weak var cmxNameTextField: UITextField!
    @IBOutlet weak var cmxIPTextField: UITextField!
    @IBOutlet weak var cmxUserNameTextField: UITextField!
    @IBOutlet weak var cmxPasswordTextField: UITextField!
    
    //MARK: - view controller life cycle methods
    
    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    
    /// <#Description#>
    func setupForNavigationBar(){
        setupNavigationBarTitle("Add Server", viewController: self)
        addNavigationBarButton(self, image: UIImage(named: "close"), title: nil, isLeft: true)
        addNavigationBarButton(self, image: UIImage(named: "doneButton"), title: nil, isLeft: false)
    }
    
    /// <#Description#>
    func registerForNotifications(){
    }
    
    /// <#Description#>
    func startupInitialisations(){
    }
    
    /// <#Description#>
    func updateUserInterfaceOnScreen(){
        
    }
    
    func onClickOfLeftBarButton(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func onClickOfRightBarButton(){
        if canContinueToCreateConfiguration(){
            resignKeyboard()
            showActivityIndicator("Fetching...")
            CMXSdk.shared.authenticate(ip: cmxIPTextField.text!, name: cmxNameTextField.text!, userName: cmxUserNameTextField.text!, password: cmxPasswordTextField.text!, completion: { (server) in
                hideActivityIndicator()
                if isNotNull(server){
                    CMXAppCommonFunctions.sharedInstance.addCMXServer(cmxServer: server!)
                    showAlert("CMX Server Added !", message: "CMX Server \(server!.version!) added successfully.\nNow you can click on server from the list and track mac address.")
                    self.dismiss(animated: true, completion: nil)
                }
                }, failure: { (error) in
                    showToast(MESSAGE_TEXT___DETAILS_NOT_VALID)
                    hideActivityIndicator()
            })
        }
    }
    
    func canContinueToCreateConfiguration()->(Bool){
        var canContinue = true
        canContinue = isInternetConnectivityAvailable(true)
        if canContinue {
            canContinue = validateName(cmxNameTextField.text as NSString?, identifier: "name")
            if canContinue == false {
                cmxNameTextField.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateIfNull(cmxIPTextField.text as AnyObject?, identifier: "ip")
            if canContinue{
                canContinue = cmxIPTextField.text!.length > 4
                if canContinue == false {
                    showToast("Please enter valid ip address")
                }
                if canContinue {
                    canContinue = Validators.isIP()(cmxIPTextField.text!)
                    if canContinue == false {
                        showToast("Please enter valid ip address")
                    }
                }
            }
            if canContinue == false {
                cmxIPTextField.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validateName(cmxUserNameTextField.text as NSString?, identifier: "username")
            if canContinue == false {
                cmxUserNameTextField.becomeFirstResponder()
            }
        }
        if canContinue {
            canContinue = validatePassword(cmxPasswordTextField.text as NSString?, identifier: "password")
            if canContinue == false {
                cmxPasswordTextField.becomeFirstResponder()
            }
        }
        if canContinue {
            let server = CMXDatabaseManager.sharedInstance.getServer(info: ["name":cmxNameTextField.text!], byKey: "name")
            if  server != nil {
                showToast(MESSAGE_TEXT___SERVER_WITH_NAME_ALREADY_ADDED)
                canContinue = false
            }
        }
        if canContinue {
            let server = CMXDatabaseManager.sharedInstance.getServer(info: ["ip":cmxIPTextField.text!], byKey: "ip")
            if  server != nil {
                showToast(MESSAGE_TEXT___SERVER_WITH_IP_ALREADY_ADDED)
                canContinue = false
            }
        }
        return canContinue
    }
    
    
    
    /// <#Description#>
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

