//
//  CMXLocateOnMapViewController.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//


import UIKit
import Foundation
//import CMXSdk
import AFNetworking
import SDWebImage
import ZKPulseView

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

/// <#Description#>
class CMXLocateOnMapViewController: UIViewController , UIScrollViewDelegate {
    
    @IBOutlet weak var xLabel: UILabel!
    @IBOutlet weak var yLabel: UILabel!
    @IBOutlet weak var motionStateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var connectedApLabel: UILabel!
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var imageViewContainerView : UIView!
    @IBOutlet var imageView : UIImageView!
    
//cmx_user_location
    
    var server : Server?
    var macAddressToTrack : String = ""
    var lastKnownLocation : CMXLocationData?
    var mapImage : UIImage?
    var lastMapImageName = ""
    var downloadingImage = false
    var locationIndicatorImageView : UIImageView!
    
    //MARK: - view controller life cycle methods
    
    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    
    /// <#Description#>
    func setupForNavigationBar(){
        setupNavigationBarTitle("Track", viewController: self)
        addNavigationBarButton(self, image: UIImage(named: "close"), title: nil, isLeft: true)
    }
    
    /// <#Description#>
    func registerForNotifications(){
        
    }
    
    /// <#Description#>
    func startupInitialisations(){
        locationIndicatorImageView = UIImageView(image: UIImage(named: "cmx_user_location.png"))
        locationIndicatorImageView.frame = CGRect(x: 0, y: 0, width: (locationIndicatorImageView.image!.size.width), height: (locationIndicatorImageView.image!.size.height))
        self.view.showActivityIndicator()
        self.locationIndicatorImageView.isHidden = true;
        self.locationIndicatorImageView.isUserInteractionEnabled = true;
        loginToServerAndStartFetchingLocationUpdate()
    }
    
    
    func reAskMacAddress(){
        if self.presentedViewController == nil {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), object: nil)
            CMXAppCommonFunctions.sharedInstance.getMacAddressAsInput(viewController: self, completionBlock: { (enteredText) in
                self.macAddressToTrack = enteredText as! String
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), object: nil)
                self.perform(#selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), with: nil, afterDelay: 1.0)
            })
        }
    }
    
    func loginToServerAndStartFetchingLocationUpdate(){
        CMXSdk.shared.authenticate(ip: server!.ip!, name: server!.name!, userName: server!.userName!, password: server!.password!, completion: {[weak self] (server) in  guard let `self` = self else { return }
            if isNotNull(server){
                CMXSdk.shared.startLocationUpdates(self.macAddressToTrack,5.0, {[weak self] (update) in guard let `self` = self else { return }
                    self.lastKnownLocation = update
                    if self.lastKnownLocation != nil {
                        self.updateParametersWithLocationUpdate()
                    }
                    }, failure: {[weak self] (error) in guard let `self` = self else { return }
                        showNotification(MESSAGE_TEXT___UNABLE_TO_TRACK_THIS_MAC_ADDRESS, showOnNavigation: false, showAsError: true)
                        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.reAskMacAddress), object: nil)
                        self.perform(#selector(CMXLocateOnMapViewController.reAskMacAddress), with: nil, afterDelay: 1.0)
                    })
            }
            }, failure: {[weak self] (error) in guard let `self` = self else { return }
                let prompt = UIAlertController(title: "Login Failed", message: MESSAGE_TEXT___UNABLE_TO_LOGIN_WITH_THIS_SERVER, preferredStyle: UIAlertControllerStyle.alert)
                prompt.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler:{[weak self] (action) -> Void in guard let `self` = self else { return }
                    self.closeAndPop()
                    }))
                prompt.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler: {[weak self] (action) -> Void in guard let `self` = self else { return }
                    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), object: nil)
                    self.perform(#selector(CMXLocateOnMapViewController.loginToServerAndStartFetchingLocationUpdate), with: nil, afterDelay: 1.0)
                    }
                ))
                self.present(prompt, animated: true, completion: nil)
            })
    }
    
    /// <#Description#>
    func updateUserInterfaceOnScreen(){
        
    }
    
    func updateParametersWithLocationUpdate() {
        xLabel.text = safeString(object: lastKnownLocation!.mapCoordinate.xValue().toString)
        yLabel.text = safeString(object: lastKnownLocation!.mapCoordinate.yValue().toString)
        timeLabel.text = safeString(object: lastKnownLocation!.mapInfo.serverTimeStamp.dateValue()?.toString(dateStyle: .medium, timeStyle: .medium))
        connectedApLabel.text = safeString(object: server!.ip)
        motionStateLabel.text = safeString(object: lastKnownLocation?.deviceActivity)
        if CMXSdk.shared.IsLoggedIn(){
            if self.lastMapImageName != lastKnownLocation!.mapInfo.floorImage.imageName {
                if self.downloadingImage == false {
                    self.downloadingImage = true
                    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXLocateOnMapViewController.resetDownloadingImageFlag), object: nil)
                    self.perform(#selector(CMXLocateOnMapViewController.resetDownloadingImageFlag), with: nil, afterDelay: 60)
                    CMXSdk.shared.getImageData(lastKnownLocation!.mapInfo.floorImage.imageName) {[weak self] (imageData) in guard let `self` = self else { return }
                        self.downloadingImage = false
                        if isNotNull(imageData){
                            self.lastMapImageName = self.lastKnownLocation!.mapInfo.floorImage.imageName
                            self.mapImage = UIImage(data: imageData as! Data)
                            self.imageViewContainerView.frame = CGRect(x: 0, y: 0, w: self.mapImage!.size.width, h: self.mapImage!.size.height)
                            self.imageView.frame = CGRect(x: 0, y: 0, w: self.mapImage!.size.width, h: self.mapImage!.size.height)
                            self.imageView.image = self.mapImage
                            self.view.hideActivityIndicator()
                            self.imageView.addSubview(self.locationIndicatorImageView)
                            self.locationIndicatorImageView.isHidden = false;
                            self.scrollView.contentSize = self.imageView!.bounds.size
                            self.updateUserCurrentLocationOnScreen()
                        }
                    }
                }
            }
        }
        updateUserCurrentLocationOnScreen()
    }
    
    func resetDownloadingImageFlag(){
        self.downloadingImage = false
    }
    
    func updateUserCurrentLocationOnScreen() {
        if self.lastKnownLocation != nil && mapImage != nil {
            print("imageView!.frame.size.width \(imageView!.frame.size.width)")
            let scaleMapImageToLocation = Double(mapImage!.size.width)/self.lastKnownLocation!.mapInfo.floorDimension.width
            let xValue = self.lastKnownLocation!.mapCoordinate.xValue() * scaleMapImageToLocation
            let yValue = self.lastKnownLocation!.mapCoordinate.yValue() * scaleMapImageToLocation
            UIView.animate(withDuration: 3, animations: {
                self.locationIndicatorImageView.center = CGPoint(x: xValue, y: yValue)
            })

        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageViewContainerView
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let invZoomScale = 1.0 / self.scrollView!.zoomScale
        locationIndicatorImageView.transform = CGAffineTransform(rotationAngle: 0).concatenating(CGAffineTransform(scaleX: invZoomScale, y: invZoomScale))
    }
    
    func closeAndPop(){
        CMXSdk.shared.logout()
        self.navigationController!.popViewController(animated: true)
    }
    
    func onClickOfLeftBarButton(){
        closeAndPop()
    }
    
    /// Description
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

