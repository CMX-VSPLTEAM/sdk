//
//  CMXServerListViewController.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//


import UIKit
import Foundation
import DZNEmptyDataSet

/// <#Description#>
class CMXServerListViewController: UIViewController , DZNEmptyDataSetSource , UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet var tableView : UITableView!
    
    var cmxServersArray = NSMutableArray()
    
    //MARK: - view controller life cycle methods
    
    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    
    /// <#Description#>
    func setupForNavigationBar(){
        setupNavigationBarTitle("CMX Servers", viewController: self)
        addNavigationBarButton(self, image: nil , title: "Add", isLeft: false)
    }
    
    /// <#Description#>
    func registerForNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(CMXServerListViewController.updateUserInterfaceOnScreen), name: NSNotification.Name(rawValue: NOTIFICATION_CMX_SERVER_LIST_UPDATED), object: nil)
    }
    
    /// <#Description#>
    func startupInitialisations(){
        tableView.emptyDataSetSource = self
    }
    
    /// <#Description#>
    func updateUserInterfaceOnScreen(){
        self.cmxServersArray = CMXDatabaseManager.sharedInstance.getAllServers()
        self.tableView.reloadData()
        self.tableView.reloadEmptyDataSet()
    }
    
    func onClickOfRightBarButton(){
        CMXAppCommonFunctions.sharedInstance.presentVC("CMXAddServerViewController", viewController: self, animated: true, modifyObject: nil)
    }
    
    //MARK: - DZNEmptyDataSet Delegate & Data Source
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_BOLD, size: 20)!]
        let message = "No Servers Configured"
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!{
        let attributes = [NSFontAttributeName : UIFont.init(name: FONT_REGULAR, size: 14)!]
        let message = "Click 'Add' button to add Cmx Server configuration"
        return NSAttributedString(string: message, attributes: attributes)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage!{
        return UIImage(named:"searchLightGray")
    }
    
    
    //MARK: - UITableView Delegate & Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cmxServersArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 66
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCell(withIdentifier: "CMXServerRepresentingTableViewCell") as! CMXServerRepresentingTableViewCell
        cell.cmxServerStatusView.makeMeRound()
        cell.cmxServerStatusView.layer.borderWidth = 2
        cell.cmxServerStatusView.layer.borderColor = UIColor.white.cgColor

        let server: Server = self.cmxServersArray[indexPath.row] as! Server
        
        cell.cmxNameLabel!.text = safeString(object: server.name)
        cell.cmxNameLabel!.adjustsFontSizeToFitWidth = true
        
        cell.cmxIPLabel!.text = safeString(object: server.ip)
        cell.cmxIPLabel!.adjustsFontSizeToFitWidth = true
        
        cell.cmxVersionLabel!.text = safeString(object: server.version)
        cell.cmxVersionLabel!.adjustsFontSizeToFitWidth = true
        
        if server.status!.contains("Running") {
            cell.cmxServerStatusView.backgroundColor = UIColor.green
        }else if server.status!.contains("UnKnown") {
            cell.cmxServerStatusView.backgroundColor = UIColor.orange
        }else if server.status!.contains("NotRunning") {
            cell.cmxServerStatusView.backgroundColor = UIColor.red
        }
        
        performAnimatedEffectType2(cell.cmxServerStatusView, repeatCount: 1024)
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?{
        return "Delete"
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            CMXDatabaseManager.sharedInstance.deleteObject(cmxServersArray.object(at: indexPath.row) as! Server)
            CMXDatabaseManager.sharedInstance.saveChangesImmediately()
            updateUserInterfaceOnScreen()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if isInternetConnectivityAvailable(true) == false {return}
        CMXAppCommonFunctions.sharedInstance.getMacAddressAsInput(viewController: self) { (macAddress) in
            if isNotNull(macAddress){
                let server = self.cmxServersArray.object(at: indexPath.row) as! Server
                CMXAppCommonFunctions.sharedInstance.pushVC("CMXLocateOnMapViewController", navigationController: self.navigationController, isRootViewController: false, animated: true) { (viewController) in
                    (viewController as! CMXLocateOnMapViewController).server = server
                    (viewController as! CMXLocateOnMapViewController).macAddressToTrack = macAddress! as! String
                }
            }
        }
    }
    
    /// <#Description#>
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

