//
//  CMXServerRepresentingTableViewCell.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//

import UIKit
import CoreMotion

/// <#Description#>
class CMXServerRepresentingTableViewCell: UITableViewCell {
    @IBOutlet weak var cmxNameLabel: UILabel!
    @IBOutlet weak var cmxIPLabel: UILabel!
    @IBOutlet weak var cmxVersionLabel: UILabel!
    @IBOutlet weak var cmxServerStatusView: UIView!
}
