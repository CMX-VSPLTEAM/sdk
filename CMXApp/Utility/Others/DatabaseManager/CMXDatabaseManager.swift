//
//  CMXDatabaseManager.swift
//  CMXApp
//
//  Created by Cisco on 02/10/16.
//  Copyright © 2016 Cisco. All rights reserved.
//

//MARK: - CMXDatabaseManager : This class handles communication of application with its database (Core Data).

import Foundation
import UIKit
import MagicalRecord
import CoreData

//MARK: - Completion block
typealias CMXDMCompletionBlock = (_ returnedData :AnyObject?) ->()

class CMXDatabaseManager: NSObject{
    var completionBlock: CMXDMCompletionBlock?
    var managedObjectContext : NSManagedObjectContext?
    /// Description
    static let sharedInstance : CMXDatabaseManager = {
        let instance = CMXDatabaseManager()
        return instance
    }()
    
    func setupCoreDataDatabase() {
        let isDataBaseValid = checkAndValidateDatabase()
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: self.dbStore())
        self.managedObjectContext = NSManagedObjectContext.mr_default()
        if isDataBaseValid == false {
            resetDatabase()
        }
    }
    
    /**
     * Server
     */
    func addServer(info:NSDictionary,services:[NSDictionary]){
        var server = getServer(info: info, byKey: "name")
        if isNull(server){
            server = getServer(info: info, byKey: "ip")
        }
        if isNull(server){
            server = Server.mr_createEntity(in: managedObjectContext!)
        }
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "name", destinationDictionary: dataToSet, destinationKey: "name", methodName:#function,asString:true)
        copyData(info, sourceKey: "ip", destinationDictionary: dataToSet, destinationKey: "ip", methodName:#function,asString:true)
        copyData(info, sourceKey: "userName", destinationDictionary: dataToSet, destinationKey: "userName", methodName:#function,asString:true)
        copyData(info, sourceKey: "password", destinationDictionary: dataToSet, destinationKey: "password", methodName:#function,asString:true)
        copyData(info, sourceKey: "version", destinationDictionary: dataToSet, destinationKey: "version", methodName:#function,asString:true)
        copyData(info, sourceKey: "status", destinationDictionary: dataToSet, destinationKey: "status", methodName:#function,asString:true)
        server!.services = NSOrderedSet()
        if services != nil {
            for service in services {
                addService(info: service, to: server!)
            }
        }
        server?.setValuesForKeys((dataToSet as? [String:AnyObject])!)
        saveChanges()
    }
    
    func getServer(info:NSDictionary,byKey:String)->Server?{
        return Server.mr_findFirst(byAttribute: byKey, withValue: info.object(forKey: byKey)!)
    }
    
    func getAllServers()->NSMutableArray{
        return (Server.mr_findAll()! as [NSManagedObject]) as! NSMutableArray
    }
    
    /**
     * Service
     */
    func addService(info:NSDictionary,to:Server){
        let service = Service.mr_createEntity(in: managedObjectContext!)
        let dataToSet = NSMutableDictionary()
        copyData(info, sourceKey: "dnsName", destinationDictionary: dataToSet, destinationKey: "dnsName", methodName:#function,asString:true)
        copyData(info, sourceKey: "lifeCycleStatus", destinationDictionary: dataToSet, destinationKey: "lifeCycleStatus", methodName:#function,asString:true)
        copyData(info, sourceKey: "nodeName", destinationDictionary: dataToSet, destinationKey: "nodeName", methodName:#function,asString:true)
        copyData(info, sourceKey: "port", destinationDictionary: dataToSet, destinationKey: "port", methodName:#function,asString:true)
        copyData(info, sourceKey: "properties", destinationDictionary: dataToSet, destinationKey: "properties", methodName:#function,asString:true)
        copyData(info, sourceKey: "serviceName", destinationDictionary: dataToSet, destinationKey: "serviceName", methodName:#function,asString:true)
        copyData(info, sourceKey: "type", destinationDictionary: dataToSet, destinationKey: "type", methodName:#function,asString:true)
        copyData(info, sourceKey: "weight", destinationDictionary: dataToSet, destinationKey: "weight", methodName:#function,asString:true)
        service!.setValuesForKeys((dataToSet as? [String:AnyObject])!)
        to.addToServices(service!)
        saveChanges()
    }

    func checkAndValidateDatabase()->Bool{
        let savedBuildInformation = UserDefaults.standard.object(forKey: APP_UNIQUE_BUILD_IDENTIFIER) as? String
        if isNotNull(savedBuildInformation as AnyObject?){
            if savedBuildInformation! == ez.appVersionAndBuild{
                // all is well , dont worry
            }else{
                // reset every thing , please
                deleteCompleteDatabaseFile()
                UserDefaults.standard.set(ez.appVersionAndBuild, forKey: APP_UNIQUE_BUILD_IDENTIFIER)
                return false
            }
        }else{
            deleteCompleteDatabaseFile()
            UserDefaults.standard.set(ez.appVersionAndBuild, forKey: APP_UNIQUE_BUILD_IDENTIFIER)
            return false
        }
        return true
    }
    
    func deleteCompleteDatabaseFile() {
        let dbStore = self.dbStore()
        let url = NSPersistentStore.mr_url(forStoreName: dbStore)
        let walURL = url!.deletingPathExtension().appendingPathExtension("sqlite-wal")
        let shmURL = url!.deletingPathExtension().appendingPathExtension("sqlite-shm")
        var removeError: NSError?
        MagicalRecord.cleanUp()
        let deleteSuccess: Bool
        do {
            try FileManager.default.removeItem(at: url!)
            try FileManager.default.removeItem(at: walURL)
            try FileManager.default.removeItem(at: shmURL)
            deleteSuccess = true
        } catch let error as NSError {
            removeError = error
            deleteSuccess = false
        }
        if deleteSuccess {
            print("database resetted successfully")
        } else {
            print("An error has occured while deleting \(dbStore)")
            print("Error description: \(removeError?.description)")
        }
    }
    
    func dbStore() -> String {
        return "\(self.bundleID()).sqlite"
    }
    
    func bundleID() -> String {
        return Bundle.main.bundleIdentifier!
    }
    
    /**
     * Common Database Operations
     */
    
    func deleteObject(_ object:NSManagedObject){
        managedObjectContext?.delete(object)
        saveChanges()
    }
    
    func resetDatabase(){
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
        saveChanges()
        UserDefaults.standard.set(ez.appVersionAndBuild, forKey: APP_UNIQUE_BUILD_IDENTIFIER)
        setDeviceToken("PLACEHOLDER")
    }
    
    func saveChanges(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(CMXDatabaseManager.saveChangesPrivate), object: nil)
        self.perform(#selector(CMXDatabaseManager.saveChangesPrivate), with: nil, afterDelay:0.3)
    }
    
    func saveChangesPrivate(){
        performOnMainThreadWithOptimisation({[weak self] (returnedData) -> () in guard let `self` = self else { return }
            self.managedObjectContext?.mr_saveToPersistentStoreAndWait()
            })
    }
    
    func saveChangesImmediately(){
        self.managedObjectContext?.mr_saveToPersistentStoreAndWait()
    }
}

